#include <QLineEdit>
#include <QSettings>
#include <QIntValidator>
#include "stdint.h"
#include <QHBoxLayout>
#include <QFont>
#include <QLabel>
#include <QKeyEvent>
#include <QPushButton>
#include <QDialogButtonBox>

#include "AddressOffsetDialog.h"
#include "Settings.h"

//
AddressOffsetDialog::AddressOffsetDialog(QWidget *parent)
  : QDialog(parent)
{
    Settings settings;
    auto address_offset = 0;
    auto pMainLayout   = new QVBoxLayout();
    auto pIpLayout     = new QHBoxLayout();
    auto pLabel        = new QLabel("address offset: ");
    auto buttonBox     = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    setLayout(pMainLayout);

    pIpLayout->addWidget(pLabel);

    m_pLineEdit = new QLineEdit();

    QRegExp rx("^-?(0x-?[0-9a-fA-F]{1,8}|[0-9]{1,9})$");
    auto validator = new QRegExpValidator(rx, m_pLineEdit);
    m_pLineEdit->setValidator(validator);
    m_pLineEdit->setMaximumWidth(150);

    auto font = m_pLineEdit->font();
    font.setStyleHint(QFont::Monospace);
    font.setFixedPitch(true);
    m_pLineEdit->setFont(font);
    m_pLineEdit->setAlignment(Qt::AlignCenter);

    auto offset = Settings().addressOffset();

    m_pLineEdit->setText(offset);

    pIpLayout->addWidget(m_pLineEdit);

    pIpLayout->addStretch();

    pMainLayout->addLayout(pIpLayout);
    pMainLayout->addWidget(buttonBox);

    connect(buttonBox, &QDialogButtonBox::accepted, this, &AddressOffsetDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &AddressOffsetDialog::reject);
}

//
AddressOffsetDialog::~AddressOffsetDialog()
{
}

//
void AddressOffsetDialog::accept()
{
    auto offset = m_pLineEdit->text();

    Settings().setAddressOffset(offset);

    QDialog::accept();
}

