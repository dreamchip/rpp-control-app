#ifndef ADDRESS_OFFSET_DIALOG_H
#define ADDRESS_OFFSET_DIALOG_H

#include <QDialog>

class QLineEdit;


class AddressOffsetDialog : public QDialog
{
    Q_OBJECT

  public:
    explicit AddressOffsetDialog(QWidget *parent = nullptr);
    virtual ~AddressOffsetDialog();

    void accept() override;

  private:
    enum
    {
        MAX_DIGITS_HEX = 8,
        MAX_DIGITS_DEC = 12,
    };

    QLineEdit* m_pLineEdit;
};

#endif // ADDRESS_OFFSET_DIALOG_H
