@ECHO OFF
%~d0

set target="release"
set dbg=""

set base_dir=%~dp0
set build_dir=build
if NOT "%1"=="" set build_dir=%1

pushd %base_dir%

call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build\vcvars64.bat"

@ECHO OFF

rmdir /S /Q %build_dir%
mkdir %build_dir%
mkdir %build_dir%\%target%
mkdir %build_dir%\%target%\imageformats
mkdir %build_dir%\%target%\platforms
mkdir %build_dir%\%target%\iconengines
mkdir %build_dir%\images

git_rev.py -f %%Y%%m%%d -o %build_dir%/git_revision.h

cd %build_dir%

cmake -G "NMake Makefiles JOM" -DCMAKE_BUILD_TYPE=%target% ..
jom -j4

copy %base_dir%\doc\readme.txt                                      .\%target%\
copy %base_dir%\doc\rpp_simulator_gui_manual.pdf                    .\%target%\
copy rpp-control-app.exe                                            .\%target%\

copy %QTDIR%\bin\Qt5Core%dbg%.dll                                   .\%target%\
copy %QTDIR%\bin\Qt5Gui%dbg%.dll                                    .\%target%\
copy %QTDIR%\bin\Qt5Network%dbg%.dll                                .\%target%\
copy %QTDIR%\bin\Qt5QML%dbg%.dll                                    .\%target%\
copy %QTDIR%\bin\Qt5QMLModels%dbg%.dll                              .\%target%\
copy %QTDIR%\bin\Qt5QmlWorkerScript%dbg%.dll                        .\%target%\
copy %QTDIR%\bin\Qt5Quick%dbg%.dll                                  .\%target%\
copy %QTDIR%\bin\Qt5QuickWidgets%dbg%.dll                           .\%target%\
copy %QTDIR%\bin\Qt5Svg%dbg%.dll                                    .\%target%\
copy %QTDIR%\bin\Qt5Widgets%dbg%.dll                                .\%target%\

copy %QTDIR%\bin\d3dcompiler_47.dll                                 .\%target%\
copy %QTDIR%\bin\libEGL%dbg%.dll                                    .\%target%\
copy %QTDIR%\bin\libGLESv2%dbg%.dll                                 .\%target%\

xcopy /E %QTDIR%\qml\QtQml                                          .\%target%\QtQml\
xcopy /E %QTDIR%\qml\QtQuick                                        .\%target%\QtQuick\
xcopy /E %QTDIR%\qml\QtQuick.2                                      .\%target%\QtQuick.2\
xcopy /E %QTDIR%\qml\QtGraphicalEffects                             .\%target%\QtGraphicalEffects\

rem del /S .\%target%\*.pdb .\%target%\*d.dll

copy %QTDIR%\qml\QtQuick\Extras\qtquickextrasplugin%dbg%.dll        .\%target%\
copy %QTDIR%\qml\QtQuick\Window.2\windowplugin%dbg%.dll             .\%target%\
copy %QTDIR%\qml\QtQuick\Controls\qtquickcontrolsplugin%dbg%.dll    .\%target%\
copy %QTDIR%\qml\QtQuick\Layouts\qquicklayoutsplugin%dbg%.dll       .\%target%\
copy %QTDIR%\qml\QtQml\Models.2\modelsplugin%dbg%.dll               .\%target%\

copy %QTDIR%\plugins\imageformats\qtga%dbg%.dll                     .\%target%\imageformats
copy %QTDIR%\plugins\imageformats\qtiff%dbg%.dll                    .\%target%\imageformats
copy %QTDIR%\plugins\imageformats\qwbmp%dbg%.dll                    .\%target%\imageformats
copy %QTDIR%\plugins\imageformats\qwebp%dbg%.dll                    .\%target%\imageformats
copy %QTDIR%\plugins\imageformats\qgif%dbg%.dll                     .\%target%\imageformats
copy %QTDIR%\plugins\imageformats\qicns%dbg%.dll                    .\%target%\imageformats
copy %QTDIR%\plugins\imageformats\qico%dbg%.dll                     .\%target%\imageformats
copy %QTDIR%\plugins\imageformats\qjpeg%dbg%.dll                    .\%target%\imageformats
copy %QTDIR%\plugins\imageformats\qsvg%dbg%.dll                     .\%target%\imageformats
copy %QTDIR%\plugins\platforms\qoffscreen%dbg%.dll                  .\%target%\platforms
copy %QTDIR%\plugins\platforms\qwindows%dbg%.dll                    .\%target%\platforms
copy %QTDIR%\plugins\platforms\qdirect2d%dbg%.dll                   .\%target%\platforms
copy %QTDIR%\plugins\platforms\qminimal%dbg%.dll                    .\%target%\platforms
copy %QTDIR%\plugins\iconengines\qsvgicon%dbg%.dll                  .\%target%\iconengines

rem FOR /F "tokens=* USEBACKQ" %%F IN (`"git log -n 1 --format=%%cd.%%h --date=format:%%Y%%m%%d"`) DO (SET revision=%%F)
FOR /F "tokens=* USEBACKQ" %%F IN (`"git log -n 1 --format=%%h"`) DO (SET revision=%%F)

"C:\Program Files\7-Zip\7z.exe" a -r rpp-control-app-gitr%revision%.zip %target%\*.*

popd

pause
