#ifndef CLASS_IDS_H
#define CLASS_IDS_H

#define MAKE_CLASS_ID_VALUE(x)   (  ((uint32_t)('0'+ (x%10)) << 24)  \
                                  | ((uint32_t)('0'+ (x/10)) << 16)  \
                                  | ((uint32_t)('L'        ) <<  8)  \
                                  | ((uint32_t)('C'        ) <<  0) )
   
#define CLASS_ID_LOGGER                     MAKE_CLASS_ID_VALUE( 1)
#define CLASS_ID_TIME_MEASUREMENT           MAKE_CLASS_ID_VALUE( 2)
#define CLASS_ID_3                          MAKE_CLASS_ID_VALUE( 3)
#define CLASS_ID_CENTRAL_WIDGET             MAKE_CLASS_ID_VALUE( 4)
#define CLASS_ID_APPLICATION                MAKE_CLASS_ID_VALUE( 5)
#define CLASS_ID_REST                       MAKE_CLASS_ID_VALUE( 6)
#define CLASS_ID_LISTMODEL                  MAKE_CLASS_ID_VALUE( 7)
#define CLASS_ID_TABLEMODEL                 MAKE_CLASS_ID_VALUE( 8)
#define CLASS_ID_REGISTER                   MAKE_CLASS_ID_VALUE( 9)
#define CLASS_ID_FUNCTION                   MAKE_CLASS_ID_VALUE(10)
#define CLASS_ID_PARAMETER                  MAKE_CLASS_ID_VALUE(11)
#define CLASS_ID_SERVER_IDENT               MAKE_CLASS_ID_VALUE(12)
#define CLASS_ID_SLICE                      MAKE_CLASS_ID_VALUE(13)
#define CLASS_ID_14                         MAKE_CLASS_ID_VALUE(14)
#define CLASS_ID_15                         MAKE_CLASS_ID_VALUE(15)
#define CLASS_ID_16                         MAKE_CLASS_ID_VALUE(16)
#define CLASS_ID_17                         MAKE_CLASS_ID_VALUE(17)
#define CLASS_ID_QUICK_APP                  MAKE_CLASS_ID_VALUE(18)
#define CLASS_ID_RPP_REGISTER_MODEL         MAKE_CLASS_ID_VALUE(19)
#define CLASS_ID_RPP_FUNCTION_MODEL         MAKE_CLASS_ID_VALUE(20)
#define CLASS_ID_MAIN_WINDOW                MAKE_CLASS_ID_VALUE(21)
#define CLASS_ID_CONFIG_APP                 MAKE_CLASS_ID_VALUE(22)
#define CLASS_ID_FILTER_VIEW                MAKE_CLASS_ID_VALUE(23)
#define CLASS_ID_TABBED_DOCK_WIDGET         MAKE_CLASS_ID_VALUE(24)
#define CLASS_ID_25                         MAKE_CLASS_ID_VALUE(25)
#define CLASS_ID_26                         MAKE_CLASS_ID_VALUE(26)
#define CLASS_ID_INSTANCE                   MAKE_CLASS_ID_VALUE(27)
#define CLASS_ID_28                         MAKE_CLASS_ID_VALUE(28)
#define CLASS_ID_29                         MAKE_CLASS_ID_VALUE(29)


#endif // #ifndef CLASS_IDS_H
