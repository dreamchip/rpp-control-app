#include <QJsonArray>
#include "Function.h"



////////////////////////////////////////////////////////////////////////////////
// class Parameter

//
Parameter::Parameter(const QJsonObject& jsonObject)
{
    fromJsonObj(jsonObject);
}

//
void Parameter::fromJsonObj(const QJsonObject& jsonObject)
{
    auto name  = jsonObject["name"       ].toString();
    auto mode  = jsonObject["mode"       ].toString();
    auto type  = jsonObject["type"       ].toString();
    auto def   = jsonObject["default"    ].toString();
    auto value = jsonObject["value"      ].toString();
    auto desc  = jsonObject["description"].toArray();

    if (name  != "")  { m_Name    = name ; }
    if (mode  != "")  { m_Mode    = mode ; }
    if (type  != "")  { m_Type    = type ; }
    if (def   != "")  { m_Default = def  ; }
    if (value != "")  { m_Value   = value; }

    for (const QJsonValue& d : desc)
    {
        m_Descr += d.toString();
        m_Descr += "\n";
    }
}

//
void Parameter::toJsonObj(QJsonObject& jsonObject, bool inclValues) const
{
    jsonObject.insert("mode" , m_Mode);
    jsonObject.insert("name" , m_Name);
    jsonObject.insert("type" , m_Type);
    if (inclValues)
    {
        jsonObject.insert("value", m_Value);
    }
}


////////////////////////////////////////////////////////////////////////////////
// class Function

//
Function::Function(const QJsonObject& jsonObject, QString blockName, uint16_t blockIdx, uint16_t funcIdx)
  : m_BlockName(blockName)
  , m_BlockIdx (blockIdx)
  , m_FuncIdx  (funcIdx)
{
    m_FuncName  = jsonObject["name"       ].toString();
    auto params = jsonObject["parameter"  ].toArray();
    auto desc   = jsonObject["description"].toArray();

    for (const auto& param: params)
    {
        std::shared_ptr<Parameter> parameter(new Parameter(param.toObject()));
        m_ParameterList.push_back(parameter);
    }

    for (const QJsonValue& d : desc)
    {
        m_Descr += d.toString();
        m_Descr += "\n";
    }
}

//
Function::~Function()
{
    m_ParameterList.clear();
}

//
void Function::toJsonObj(QJsonObject& jsonObject, bool inclValues) const
{
    QJsonArray  params;

    for (const auto& parameter: m_ParameterList)
    {
        QJsonObject  param_o;
        parameter->toJsonObj(param_o, inclValues);
        params.append(param_o);
    }

    jsonObject.insert("name", m_FuncName);
    jsonObject.insert("parameter", params);
}

//
void Function::setParameterValue(const QString& name, const QString& value)
{
    for (const auto& parameter: m_ParameterList)
    {
        if (parameter->m_Name == name)
        {
            parameter->m_Value = value;
            return;
        }
    }
}

//
void Function::updateParameter(const QJsonObject& jsonObject)
{
    auto name = jsonObject["name"].toString();

    for (const auto& parameter: m_ParameterList)
    {
        if (parameter->m_Name == name)
        {
            parameter->fromJsonObj(jsonObject);
            return;
        }
    }

    std::shared_ptr<Parameter> parameter(new Parameter(jsonObject));
    m_ParameterList.push_back(parameter);
}

void Function::getParamList(QList<QStringList>& list)
{
    for (auto param : m_ParameterList)
    {
        list.append({param->m_Name, param->m_Mode, param->m_Type, param->m_Value});
    }
}

void Function::getParamDescr(QStringList& list)
{
    for (auto param : m_ParameterList)
    {
        list.append(param->m_Descr);
    }
}

bool Function::hasRWParams()
{
    bool result = false;

    for (auto param : m_ParameterList)
    {
        result |= (param->m_Mode == "rw");
    }

    return result;
}

