#ifndef FUNCTION_H
#define FUNCTION_H

#include <memory>
#include <cstdint>
#include <vector>
#include <QString>
#include <QMap>
#include <QJsonObject>
#include "ClassIDs.h"


class Parameter
{
    friend class Function;

  public:
    Parameter(const QJsonObject& jsonObject);

    void fromJsonObj(const QJsonObject& jsonObject);
    void toJsonObj  (      QJsonObject& jsonObject, bool inclValues = true) const;

  protected:
    const uint32_t  m_CLASS_ID = CLASS_ID_PARAMETER;
    QString         m_Name;
    QString         m_Mode;
    QString         m_Type;
    QString         m_Default;
    QString         m_Value;
    QString         m_Descr;
};


class Function
{
  public:
    Function(const QJsonObject& jsonObject, QString blockName, uint16_t blockIdx, uint16_t funcIdx);

    virtual ~Function();

    void toJsonObj(QJsonObject& jsonObject, bool inclValues = true) const;

    void setParameterValue(const QString& name, const QString& value);
    void updateParameter(const QJsonObject& jsonObject);

    void getParamList(QList<QStringList>& list);

    void getParamDescr(QStringList& list);

    bool hasRWParams();

    const QString& blockName  ()  const  { return m_BlockName; }
    const QString& funcName   ()  const  { return m_FuncName ; }
    const QString& description()  const  { return m_Descr    ; }
    uint16_t       blockIdx   ()  const  { return m_BlockIdx ; }
    uint16_t       funcIdx    ()  const  { return m_FuncIdx  ; }
    uint32_t       id         ()  const  { return ((uint32_t)m_BlockIdx << 16) + m_FuncIdx; }
    uint32_t       numParams  ()  const  { return (uint32_t)m_ParameterList.size(); }

  protected:
    typedef std::vector<std::shared_ptr<Parameter>>     ParameterList;

  protected:
    const uint32_t  m_CLASS_ID = CLASS_ID_FUNCTION;
    QString         m_BlockName;
    QString         m_FuncName;
    QString         m_Descr;
    uint16_t        m_BlockIdx;
    uint16_t        m_FuncIdx;
    ParameterList   m_ParameterList;
};

#endif // FUNCTION_H
