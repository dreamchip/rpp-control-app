#include "Instance.h"
#include "Logger.h"

Instance::Instance(const QString& file, const QString& name, uint32_t offset, uint32_t id)
  : m_pLogger(new DCT::Logger(name.toStdString().c_str()))
  , m_File  (file)
  , m_Name  (name)
  , m_Offset(offset)
  , m_Id    (id)
{
    (void) m_CLASS_ID;

    //LOG_INFO("", 0);
}

Instance::~Instance()
{
    //LOG_INFO("", 0);

    delete m_pLogger;
}
