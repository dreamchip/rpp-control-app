#ifndef INSTANCE_H
#define INSTANCE_H

#include <cstdint>
#include <QString>
#include "ClassIDs.h"


namespace DCT { class Logger; }


class Instance
{
  public:
    Instance(const QString& file, const QString& name, uint32_t offset, uint32_t id);

    virtual ~Instance();

    const QString& file()    { return m_File; }
    const QString& name()    { return m_Name; }
    uint32_t       offset()  { return m_Offset; }
    uint32_t       id()      { return m_Id; }

  private:
    const uint32_t  m_CLASS_ID = CLASS_ID_INSTANCE;
    DCT::Logger*    m_pLogger;
    QString         m_File;
    QString         m_Name;
    uint32_t        m_Offset;
    uint32_t        m_Id;
};

#endif // INSTANCE_H
