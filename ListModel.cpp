#include "ListModel.h"
#include "Logger.h"

ListModel::ListModel(QObject *parent, const char *moduleName)
  : QAbstractListModel(parent)
  , m_pLogger(new DCT::Logger(moduleName))
{
    LOG_INFO("", "");
}

ListModel::~ListModel()
{
    m_entries.clear();

    LOG_INFO("", "");
    delete m_pLogger;
}

void ListModel::append(const QString &type, const QString &text)
{
    //LOG_INFO("", "");

    beginInsertRows(QModelIndex(), rowCount(QModelIndex()), rowCount(QModelIndex()));
    m_entries.append({type, text});
    endInsertRows();
}

void ListModel::remove(int row)
{
    LOG_INFO("", "");

    if (row < 0 || row >= m_entries.count())
    {
        return;
    }

    beginRemoveRows(QModelIndex(), row, row);
    m_entries.removeAt(row);
    endRemoveRows();
}

void ListModel::remove_first()
{
    LOG_INFO("", "");

    if (m_entries.isEmpty())
    {
        return;
    }

    beginRemoveRows(QModelIndex(), 0, 0);
    m_entries.removeLast();
    endRemoveRows();
}

void ListModel::clean()
{
    //LOG_INFO("", "");

    if (m_entries.isEmpty())
    {
        return;
    }

    beginRemoveRows(QModelIndex(), 0, m_entries.count()-1);
    m_entries.clear();
    endRemoveRows();
}

int ListModel::rowCount(const QModelIndex &parent) const
{
    //LOG_INFO("", "");

    Q_UNUSED(parent);
    return m_entries.size();
}

QVariant ListModel::data(const QModelIndex &index, int role) const
{
    //LOG_INFO("", "");

    if (index.row() < 0 || index.row() >= m_entries.size())
    {
        return QVariant();
    }

    const ListEntry &entry = m_entries[index.row()];
    if (role == TypeRole) {
        return entry.type;
    }
    if (role == TextRole) {
        return entry.text;
    }
    if (role == TypeTextRole) {
        return entry.type + ": " + entry.text;
    }

    return QVariant();
}

QHash<int, QByteArray> ListModel::roleNames() const
{
    //LOG_INFO("", "");

    QHash<int, QByteArray> roles;

    roles[TypeRole]     = "type";
    roles[TextRole]     = "log";
    roles[TypeTextRole] = "type_text";

    return roles;
}
