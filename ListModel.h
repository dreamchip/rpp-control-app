#ifndef LIST_MODEL_H
#define LIST_MODEL_H

#include <QAbstractListModel>
#include "ClassIDs.h"


namespace DCT { class Logger; }


class ListModel : public QAbstractListModel
{
    Q_OBJECT

private:
    struct ListEntry
    {
        QString type;
        QString text;
    };
    QList<ListEntry> m_entries;

public:
    enum Roles {
        TypeRole = Qt::UserRole + 1,
        TextRole,
        TypeTextRole
    };

    ListModel(QObject *parent = nullptr, const char *moduleName = "");

    virtual ~ListModel();

    Q_INVOKABLE void append(const QString &type, const QString &text);
    Q_INVOKABLE void remove(int row);
    Q_INVOKABLE void remove_first();
    Q_INVOKABLE void clean();
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

protected:
    const uint32_t  m_CLASS_ID = CLASS_ID_LISTMODEL;
    DCT::Logger*    m_pLogger;

    QHash<int, QByteArray> roleNames() const;

};

#endif // LIST_MODEL_H
