/******************************************************************************
*
* Copyright 2018,2019, Dream Chip Technologies GmbH. All rights reserved.
* No part of this work may be reproduced, modified, distributed, transmitted,
* transcribed, or translated into any language or computer format, in any form
* or by any means without written permission of:
* Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
* Germany
*
*****************************************************************************/
/**
* @file   LogString.h
*
* @brief  
*
*****************************************************************************/
#ifndef LOG_STRING_H
#define LOG_STRING_H

#include "inttypes.h"


namespace DCT
{

#define MAX_LOG_STR_BUF_LEN     8*1024
#define LOG_STR_REM(n)          (MAX_LOG_STR_BUF_LEN - 1 - (n))

class LogStringPool;  // feature not yet implemented

typedef struct _LogString
{
    LogStringPool*  m_pPool;
    uint32_t        m_Id;
    uint32_t        m_CurrPos;
    char            m_String[MAX_LOG_STR_BUF_LEN];
} LogString;


} // namespace A10CC

#endif // #ifndef LOG_STRING_H
