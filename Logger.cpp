/******************************************************************************
*
* Copyright 2018,2019, Dream Chip Technologies GmbH. All rights reserved.
* No part of this work may be reproduced, modified, distributed, transmitted,
* transcribed, or translated into any language or computer format, in any form
* or by any means without written permission of:
* Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
* Germany
*
*****************************************************************************/
/**
* @file   Logger.cpp
*
* @brief  
*
*****************************************************************************/
#include <cstdarg>
#include <cstring>
#include <algorithm>
#include "Logger.h"
#include "TimeMeasurement.h"
#include "ClassIDs.h"

#ifdef min
#undef min
#endif

namespace DCT
{

// static initializer
TimeMeasurement*    Logger::s_pTimer            = nullptr;
uint32_t            Logger::s_StaticInstance    = 0;
uint32_t            Logger::s_InstanceCounter   = 0;
uint32_t            Logger::s_DestroyedCounter  = 0;
bool                Logger::s_Initialized       = false;
char                Logger::s_ModuleName[MAX_MODULE_NAME_LEN+1] = "0";

///////////////////////////////////////////////////////////////////////////////////////////////////

//
void Logger::init(uint32_t instance) // static
{
    if (!s_Initialized)
    {
        s_pTimer         = new TimeMeasurement();
        s_Initialized    = true;
        s_StaticInstance = instance;
        
        SNPRINTF(s_ModuleName, MAX_MODULE_NAME_LEN, "%d", instance);

        LOG_S_TRACE(s_ModuleName, "", "");
    }
    else
    {
        LOG_S_WARN(s_ModuleName, "Logger already initialized", "");
    }
}

//
void Logger::exit() // static
{
    if (s_Initialized)
    {
        if (s_InstanceCounter - s_DestroyedCounter == 0)
        {
            delete s_pTimer;
            s_pTimer = nullptr;

            s_Initialized = false;

            LOG_S_INFO(s_ModuleName, "success", "");
        }
        else
        {
            LOG_S_WARN(s_ModuleName, "nothing done; there are still some instances", "");
        }
    }
    else
    {
        LOG_S_WARN(s_ModuleName, "Logger was not initialized", "");
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

// Constructor
// @param moduleName The name of the related modul - which is printed together with all log messages
Logger::Logger(const char *moduleName)
  : m_CLASS_ID(CLASS_ID_LOGGER)
  , m_instanceID(++s_InstanceCounter)
  , m_ModuleName("")
  , m_LogLevelMask(LL_ALL)
{
    (void) m_CLASS_ID;

    if (!s_Initialized)
    {
        Logger::init();
    }

    setModuleName(moduleName);

    //LOG_S_TRACE(m_ModuleName, "%d %3d 0x%p", s_StaticInstance, m_instanceID, this);
}

// Destructor
Logger::~Logger()
{
    s_DestroyedCounter++;
    //LOG_S_TRACE(m_ModuleName, "%3d 0x%p (%d/%d)", m_instanceID, this, s_DestroyedCounter, s_InstanceCounter);

    if (s_InstanceCounter - s_DestroyedCounter == 0)
    {
        Logger::exit();
    }
}

//
void Logger::setLogLevelMask(uint32_t mask)
{ 
    m_LogLevelMask = mask;
}

//
uint32_t Logger::getLogLevelMask() const
{ 
    return m_LogLevelMask;
}

//
void Logger::setModuleName(const char* moduleName)
{ 
    memcpy(m_ModuleName, moduleName, MAX_MODULE_NAME_LEN-1);
    m_ModuleName[MAX_MODULE_NAME_LEN-1] = 0;
}

//
const char* Logger::getModuleName() const
{ 
    return m_ModuleName;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

//
void Logger::appendLevel(LogString* pLogString, LogLevel level) // static
{
    uint32_t  pos = pLogString->m_CurrPos;
    char*     str = pLogString->m_String;

    switch (level)
    {
        case LL_ERROR:    pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "ERROR:   ");  break;
        case LL_WARNING:  pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "WARNING: ");  break;
        case LL_INFO:     pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "INFO:    ");  break;
        case LL_TRACE:    pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "TRACE:   ");  break;
        default:          pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "UNKNOWN: ");  break;
    }
}

//
void Logger::appendTime(LogString* pLogString) // static
{
    uint32_t  pos = pLogString->m_CurrPos;
    char*     str = pLogString->m_String;

    if (s_pTimer)
    {
        uint64_t  time = s_pTimer->getMicrosecondsSinceStart();
        uint64_t  sec  = time / 1000000;
        uint64_t  usec = time % 1000000;
        pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "%06llu,%06llu ", (unsigned long long)sec, (unsigned long long)usec);
    }
    else
    {
        pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "              ");
    }
}

//
void Logger::appendThreadId(LogString* pLogString) // static
{
  #if WIN32
    int threadID = GetCurrentThreadId();
  #else
    int threadID = 0;   // TODO: use correct function for Linux/Mac
  #endif

    uint32_t  pos = pLogString->m_CurrPos;
    char*     str = pLogString->m_String;

    pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "%04x ", threadID);
}

//
void Logger::appendModule(LogString* pLogString, const char* module_name) // static
{
    uint32_t  pos = pLogString->m_CurrPos;
    char*     str = pLogString->m_String;

    pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "%-*s ", MAX_MODULE_NAME_LEN, module_name);
}

//
void Logger::appendFunction(LogString* pLogString, const char* func, bool addParen) // static
{
    uint32_t  pos = pLogString->m_CurrPos;
    char*     str = pLogString->m_String;

    if (addParen)
    {
        pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "%s() ", func);
    }
    else
    {
        pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "%s ", func);
    }
}

//
void Logger::appendLineEnd(LogString* pLogString) // static
{
    uint32_t  pos = pLogString->m_CurrPos;
    char*     str = pLogString->m_String;

    if ((pos == 0) || (str[pos-1] != '\n') )
    {
        pLogString->m_CurrPos += SNPRINTF(&str[pos], MAX_LOG_STR_BUF_LEN - pos, "\n");
    }
}

// A method for printing dedug/logging messages
// @param moduleName    module or object name to distinguish between same function calls
// @param level         logging level (e.g. LL_ERROR, LL_WARNING etc.)
// @param levelmask     mask to be checked for level
// @param format, ...   text to be otuput in printf()-style
void Logger::log(const char* moduleName, LogLevel level, uint32_t levelMask, const char* func, const char* format, ...)  // static
{
    if (level & levelMask)
    {
        LogString  logString = { nullptr, 0, 0, "" };

        appendTime(&logString);

        if ((level & LL_SHORT) == 0)
        {
            appendThreadId(&logString);
            appendLevel   (&logString, level);
            appendModule  (&logString, moduleName);
            appendFunction(&logString, func, (bool)moduleName);
        }

        uint32_t  pos = logString.m_CurrPos;
        char*     str = logString.m_String;
        uint32_t  rem = LOG_STR_REM(pos)-2; // 2 for final "\n\0"

        va_list list;
        va_start(list, format);

        // The vsnprintf() function returns the number of chars that would get written 
        // if the buffer was long enough, not the number of chars actually written.
        uint32_t  amt = VSNPRINTF(&str[pos], rem, format, list);

        va_end(list);

        // the new position must be corrected by the minimum of the VSNPRINTF()-returned 
        // amount and the actually remaining space.
        logString.m_CurrPos += std::min(amt, rem);

        // Make sure to zero-terminate the string, which is important if VSNPRINTF() 
        // actually overflowed. in that case, it does not write a trailing NUL character.
        logString.m_String[logString.m_CurrPos] = 0;

        appendLineEnd(&logString);

        DBG_PRINT(logString.m_String);
    }
}

// just using arguments to prevent compiler warning in case of disabled log output (no LOG_INFO and LOG_TRACE in release versions)
void Logger::nolog(const char* format, ...)  // static
{
    (void)(format);
}


} // namespace A10CC
