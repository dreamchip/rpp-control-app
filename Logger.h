/******************************************************************************
*
* Copyright 2020, Dream Chip Technologies GmbH. All rights reserved.
* No part of this work may be reproduced, modified, distributed, transmitted,
* transcribed, or translated into any language or computer format, in any form
* or by any means without written permission of:
* Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
* Germany
*
*****************************************************************************/
/**
* @file   Logger.h
*
* @brief  
*
*****************************************************************************/
#ifndef LOGGER_H
#define LOGGER_H

#include <cinttypes>

#if WIN32
# include <Windows.h>
# include <stdio.h>
# define SNPRINTF       snprintf    // _snprintf
# define VSNPRINTF      vsnprintf   // _vsnprintf
# define DBG_PRINT(x)   OutputDebugStringA(x)
#else
# include <stdio.h>
# define SNPRINTF       snprintf
# define VSNPRINTF      vsnprintf
# define DBG_PRINT(x)   fprintf(stdout, "%s", x)
#endif

#include "LogString.h"

#ifndef H_FILE
typedef void*  H_FILE;              ///< handle to an opened file
#endif

namespace DCT
{

#ifndef MAX_MODULE_NAME_LEN
#define MAX_MODULE_NAME_LEN     23
#endif

#undef LOG_S_SHORT
#undef LOG_S_ERROR
#undef LOG_S_WARN
#undef LOG_S_INFO
#undef LOG_S_TRACE
#undef LOG_SHORT
#undef LOG_ERROR
#undef LOG_WARN
#undef LOG_INFO
#undef LOG_TRACE

#ifndef  LOGGER
# define LOGGER   m_pLogger
#endif

# define MODULE   LOGGER->getModuleName()
# define LL_MASK  LOGGER->getLogLevelMask()

// static versions to use when no Logger object is available
#define  LOG_S_SHORT(mod, format, ...)  DCT::Logger::log(mod   , DCT::LL_SHORT  , DCT::LL_ALL , ""          , format, __VA_ARGS__)
#define  LOG_S_ERROR(mod, format, ...)  DCT::Logger::log(mod   , DCT::LL_ERROR  , DCT::LL_ALL , __FUNCTION__, format, __VA_ARGS__)
#define  LOG_S_WARN( mod, format, ...)  DCT::Logger::log(mod   , DCT::LL_WARNING, DCT::LL_ALL , __FUNCTION__, format, __VA_ARGS__)
#if _DEBUG || DEBUG || !defined(QT_NO_DEBUG)
# define LOG_S_INFO( mod, format, ...)  DCT::Logger::log(mod   , DCT::LL_INFO   , DCT::LL_ALL , __FUNCTION__, format, __VA_ARGS__)
# define LOG_S_TRACE(mod, format, ...)  DCT::Logger::log(mod   , DCT::LL_TRACE  , DCT::LL_ALL , __FUNCTION__, format, __VA_ARGS__)
#else
# define LOG_S_INFO( mod, format, ...)  DCT::Logger::nolog(format, __VA_ARGS__) // using arguments just for compiler
# define LOG_S_TRACE(mod, format, ...)  DCT::Logger::nolog(format, __VA_ARGS__) // using arguments just for compiler
#endif

// normal version to use when Logger object is available
#define  LOG_SHORT(       format, ...)  DCT::Logger::log(MODULE, DCT::LL_SHORT  , LL_MASK     , ""          , format, __VA_ARGS__)
#define  LOG_ERROR(       format, ...)  DCT::Logger::log(MODULE, DCT::LL_ERROR  , LL_MASK     , __FUNCTION__, format, __VA_ARGS__)
#define  LOG_WARN(        format, ...)  DCT::Logger::log(MODULE, DCT::LL_WARNING, LL_MASK     , __FUNCTION__, format, __VA_ARGS__)
#if _DEBUG || DEBUG || !defined(QT_NO_DEBUG)
# define LOG_INFO(        format, ...)  DCT::Logger::log(MODULE, DCT::LL_INFO   , LL_MASK     , __FUNCTION__, format, __VA_ARGS__)
# define LOG_TRACE(       format, ...)  DCT::Logger::log(MODULE, DCT::LL_TRACE  , LL_MASK     , __FUNCTION__, format, __VA_ARGS__)
#else
# define LOG_INFO(        format, ...)  DCT::Logger::nolog(format, __VA_ARGS__) // using arguments just for compiler
# define LOG_TRACE(       format, ...)  DCT::Logger::nolog(format, __VA_ARGS__) // using arguments just for compiler
#endif


/// The different possible logging levels
enum LogLevel
{ 
    noLL_ERROR   = 0,
    noLL_WARNING = 0,
    noLL_SHORT   = 0,
    noLL_INFO    = 0,
    noLL_TRACE   = 0,
    LL_ERROR     = 0x01,
    LL_WARNING   = 0x02,
    LL_SHORT     = 0x04,  // for short debug lines with less additional info strings
    LL_INFO      = 0x08,
    LL_TRACE     = 0x10,

    LL_ALL       = 0xff
};


inline LogLevel operator|(const LogLevel& op1, const LogLevel& op2)
{
    return (LogLevel)(op1 + op2);
}

class TimeMeasurement;


class Logger
{
  public:

    Logger(const char *moduleName = "");

    ~Logger(); // not virtual

    void setLogLevelMask(uint32_t mask);
    uint32_t getLogLevelMask() const;

    void setModuleName(const char* moduleName);
    const char* getModuleName() const;

    static void init(uint32_t instance = 0);
    static void exit();
    static void setCurrTime(uint64_t usec);
    static uint64_t getCurrTime();

    static void log(const char* moduleName, LogLevel level, uint32_t levelMask, const char* func, const char* format, ...);
    static void nolog(const char* format, ...); // using arguments just for compiler 

  private:
    static void appendLevel   (LogString* pLogString, const LogLevel level);
    static void appendThreadId(LogString* pLogString);
    static void appendTime    (LogString* pLogString);
    static void appendModule  (LogString* pLogString, const char* module_name);
    static void appendFunction(LogString* pLogString, const char* func, bool addParen = true);
    static void appendLineEnd (LogString* pLogString);

  private:
    const uint32_t  m_CLASS_ID;
    uint32_t        m_instanceID;
    char            m_ModuleName[MAX_MODULE_NAME_LEN+1];
    uint32_t        m_LogLevelMask;

  private:
    static TimeMeasurement* s_pTimer;
    static uint32_t         s_StaticInstance;
    static uint32_t         s_InstanceCounter;
    static uint32_t         s_DestroyedCounter;
    static bool             s_Initialized;
    static char             s_ModuleName[MAX_MODULE_NAME_LEN+1];
};


} // namespace DCT

#endif // #ifndef LOGGER_H
