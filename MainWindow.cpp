#include <memory>
#include <QtWidgets>
#include <QQuickWidget>
#include <QTableView>
#include <QListView>
#include <QQmlContext>
#include <utility>

#include "MainWindow.h"
#include "Logger.h"
#include "StyleSheetTCO.h"
#include "ClassIDs.h"
#include "RppFunctionsModel.h"
#include "RppRegistersModel.h"
#include "ServerConfig.h"
#include "AddressOffsetDialog.h"
#include "SettingsDialog.h"
#include "Settings.h"

namespace DCT
{


MainWindow::MainWindow()
  : m_CLASS_ID          (CLASS_ID_MAIN_WINDOW)
  , m_pLogger           (new Logger("MainWindow"))
  , m_pRppFunctionsModel(new RppFunctionsModel())
  , m_pRppRegistersModel(new RppRegistersModel())
  , m_pServerConfig     (new ServerConfig(m_pRppFunctionsModel, m_pRppRegistersModel))
{
    (void) m_CLASS_ID;

    LOG_INFO("", "");

    setWindowTitle(tr("DCT RPP Control App"));

    createActions();
    createDockWidgets();

    m_pTabWidget = new QTabWidget(this);
    setCentralWidget(m_pTabWidget);

    m_pFuncView = new QQuickWidget;
    m_pFuncView->rootContext()->setContextProperty("funcModel", m_pRppFunctionsModel);
    m_pFuncView->resize(300,200);
    m_pFuncView->setResizeMode(QQuickWidget::SizeRootObjectToView);
    m_pFuncView->setSource(QUrl("qrc:/RppFunctionsView.qml"));
    m_pTabWidget->addTab(m_pFuncView, tr("Functions"));

    m_pRegView = new QQuickWidget;
    m_pRegView->rootContext()->setContextProperty("regModel", m_pRppRegistersModel);
    m_pRegView->resize(300,200);
    m_pRegView->setResizeMode(QQuickWidget::SizeRootObjectToView);
    m_pRegView->setSource(QUrl("qrc:/RppRegistersView.qml"));
    m_pTabWidget->addTab(m_pRegView, tr("Registers"));

    readSettings();

    setUnifiedTitleAndToolBarOnMac(true);

    setAcceptDrops(true);

    styleSheet0();

    statusBar()->showMessage(tr("Ready"));
}

MainWindow::~MainWindow()
{
    delete m_pRegView;
    delete m_pFuncView;
    delete m_pTabWidget;

    delete m_pServerConfig;
    delete m_pRppRegistersModel;
    delete m_pRppFunctionsModel;

    LOG_INFO("", "");

    delete m_pLogger;
}

#define CREATE_CONNECTED_ACTION(pAction, rIcon, menuText, rKeySequnce, statusTip, slot) \
{ \
    pAction = new QAction((rIcon), tr(menuText), this); \
    pAction->setShortcut(rKeySequnce); \
    pAction->setStatusTip(tr(statusTip)); \
    connect(pAction, &QAction::triggered, this, &slot); \
}

void MainWindow::createActions()
{
    LOG_INFO("", "");

    m_pToolbar = addToolBar(tr("Main"));

    m_pMenuFile = menuBar()->addMenu(tr("&File"));
    m_pMenuWin  = menuBar()->addMenu(tr("&Window"));
    m_pMenuHelp = menuBar()->addMenu(tr("&Help"));

    static const QIcon iconApplication = QIcon::fromTheme("dct-logo"        , QIcon(":/images/DCTlogo2.png"));
    static const QIcon iconQuit        = QIcon::fromTheme("application-exit", QIcon(":/images/Exit.png"));
    static const QIcon iconFwd         = QIcon::fromTheme("forward"         , QIcon(":/images/PlayerFwd.png"));
    static const QIcon iconEmpty;

    setWindowIcon(iconApplication);

    CREATE_CONNECTED_ACTION(m_pActionLoadRegisters  , iconEmpty     , "Load Registers..."       , QKeySequence(                                ), "Load registers from disk"             , MainWindow::loadRegisters   );
    CREATE_CONNECTED_ACTION(m_pActionSaveRegisters  , iconEmpty     , "Save All Registers..."   , QKeySequence(                                ), "Save all registers to disk"           , MainWindow::saveAllRegisters   );
    CREATE_CONNECTED_ACTION(m_pActionSaveProgramming, iconEmpty     , "Save Programming..."     , QKeySequence(                                ), "Save writeable registers to disk"     , MainWindow::saveWrRegisters   );
    CREATE_CONNECTED_ACTION(m_pActionLoadFunctions  , iconEmpty     , "Load Functions..."       , QKeySequence(                                ), "Load functions from disk"             , MainWindow::loadFunctions   );
    CREATE_CONNECTED_ACTION(m_pActionSaveFunctions  , iconEmpty     , "Save Functions..."       , QKeySequence(                                ), "Save functions to disk"               , MainWindow::saveFunctions   );
    CREATE_CONNECTED_ACTION(m_pActionEditSettings   , iconEmpty     , "Settings ..."            , QKeySequence(                                ), "Edit settings"                        , MainWindow::editSettings    );
    CREATE_CONNECTED_ACTION(m_pActionQuit           , iconQuit      , "E&xit"                   , QKeySequence::Quit                            , "Exit the application"                 , QWidget   ::close           );
    CREATE_CONNECTED_ACTION(m_pActionProcess        , iconFwd       , "Run processing"          , QKeySequence(Qt::ALT           +Qt::Key_R    ), "Start processing image data"          , MainWindow::startProcessing );
    CREATE_CONNECTED_ACTION(m_pActionStyleSheet0    , iconEmpty     , "Style Sheet default"     , QKeySequence(                                ), ""                                     , MainWindow::styleSheet0     );
    CREATE_CONNECTED_ACTION(m_pActionStyleSheet1    , iconEmpty     , "Style Sheet DCT"         , QKeySequence(                                ), ""                                     , MainWindow::styleSheet1     );
    CREATE_CONNECTED_ACTION(m_pActionAbout          , iconEmpty     , "&About"                  , QKeySequence::HelpContents                    , "Show the application's About box"     , MainWindow::about           );

    m_pMenuFile->addAction(m_pActionLoadFunctions);
    m_pMenuFile->addAction(m_pActionSaveFunctions);
    m_pMenuFile->addAction(m_pActionLoadRegisters);
    m_pMenuFile->addAction(m_pActionSaveRegisters);
    m_pMenuFile->addAction(m_pActionSaveProgramming);
    m_pMenuFile->addAction(m_pActionEditSettings);
    m_pMenuFile->addSeparator();
    m_pMenuFile->addAction(m_pActionQuit);

    m_pMenuWin->addAction(m_pActionStyleSheet0);
    m_pMenuWin->addAction(m_pActionStyleSheet1);

    m_pMenuHelp->addAction(m_pActionAbout);

    m_pToolbar->addAction(m_pActionProcess);
    m_pToolbar->addSeparator();
}

void MainWindow::createDockWidgets()
{
    LOG_INFO("", "");
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    LOG_INFO("", "");

    writeSettings();
    event->accept();
}

void MainWindow::loadRegisters() // menu File/Load registers
{
    LOG_INFO("", "");

    QFileDialog fileDialog(this);
    fileDialog.setWindowModality(Qt::WindowModal);
    fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    fileDialog.setFileMode(QFileDialog::ExistingFiles);
    fileDialog.setNameFilter("*.json");

    if (fileDialog.exec() == QDialog::Accepted)
    {
        QString fileName = fileDialog.selectedFiles().first();
        if (!fileName.isEmpty())
        {
            // ask user for positive or negative offset to add to register addresses
            auto adressDialog = new AddressOffsetDialog();

            adressDialog->exec();

            if (adressDialog->result() == QDialog::Accepted)
            {
                auto offsetString = Settings().addressOffset();
                int  addressOffset = 0;
                int  sign = 1;
                bool hex  = false;

                LOG_INFO("Address offset: %s", offsetString.toStdString().c_str());

                if (offsetString.startsWith(Qt::Key_Minus))
                {
                    sign = -1;
                    offsetString = offsetString.remove(0, 1);
                }

                if (offsetString.startsWith("0x"))
                {
                    offsetString = offsetString.remove(0, 2);
                    if (offsetString.startsWith(Qt::Key_Minus))
                    {
                        sign = -1;
                        offsetString = offsetString.remove(0, 1);
                    }
                    
                    addressOffset = offsetString.toInt(nullptr, 16);
                }
                else
                {
                    addressOffset = offsetString.toInt(nullptr, 10);
                }

                addressOffset *= sign;

                if (addressOffset < 0)
                {
                    LOG_INFO("Address offset: -0x%08x", -addressOffset);
                }
                else
                {
                    LOG_INFO("Address offset: 0x%08x", addressOffset);
                }

                m_pRppRegistersModel->loadRegistersFromDisk(fileName, addressOffset);
            }
        }
    }
}

void MainWindow::saveAllRegisters() // menu File/Save All Rregisters
{
    saveRegisters(true);
}


void MainWindow::saveWrRegisters() // menu File/Save Programming
{
    // remember only writeable registers.
    saveRegisters(false);
}

void MainWindow::saveRegisters(bool all) // helper function
{
    LOG_INFO("", "");

    QFileDialog dialog(this);
    //dialog.setOption(QFileDialog::DontUseSheet);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setNameFilter("*.json");
    dialog.setDefaultSuffix("json");

    if (dialog.exec() == QDialog::Accepted)
    {
        QString fileName = dialog.selectedFiles().first();
        LOG_INFO("%s", fileName.toStdString().c_str());
        if (!fileName.isEmpty())
        {
            m_pRppRegistersModel->saveRegistersToDisk(fileName, all);
        }
    }
}

void MainWindow::loadFunctions() // menu File/Load functions
{
    LOG_INFO("", "");

    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setNameFilter("*.json");

    if (dialog.exec() == QDialog::Accepted)
    {
        QString fileName = dialog.selectedFiles().first();
        if (!fileName.isEmpty())
        {
            m_pRppFunctionsModel->loadFunctionsFromDisk(fileName);
        }
    }
}

void MainWindow::saveFunctions() // menu File/Save functions
{
    LOG_INFO("", "");

    QFileDialog dialog(this);
    //dialog.setOption(QFileDialog::DontUseSheet);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setNameFilter("*.json");
    dialog.setDefaultSuffix("json");

    if (dialog.exec() == QDialog::Accepted)
    {
        QString fileName = dialog.selectedFiles().first();
        LOG_INFO("%s", fileName.toStdString().c_str());
        if (!fileName.isEmpty())
        {
            m_pRppFunctionsModel->saveFunctionsToDisk(fileName);
        }
    }
}

void MainWindow::editSettings()
{
    LOG_INFO("", "");

    auto dialog = new SettingsDialog();

    dialog->exec();

    if (dialog->result() == QDialog::Accepted)
    {
        m_pServerConfig->reset();
    }
}

void MainWindow::startProcessing()
{
    LOG_INFO("TODO: ", "");

    m_pRppFunctionsModel->startProcessing();
}

void MainWindow::setStyleSheet(std::string& style) // common for menu Window/Style Sheet [...]
{
    LOG_INFO("", "");

    QMainWindow::setStyleSheet(QString::fromStdString(style));
}

void MainWindow::styleSheet0() { setStyleSheet(m_DefaultStyle      ); } // menu Window/Style Sheet default
void MainWindow::styleSheet1() { setStyleSheet(StyleSheetTCO       ); } // menu Window/Style Sheet TCO

void MainWindow::about() // menu Help/About
{
    LOG_INFO("", "");

    QMessageBox::about(this, tr("RPP control GUI"),
                             tr("<b>%1 v%2</b><br><br>"
                                "Copyright 2021<br><b>%3</b><br><br>"
                                "connected to server:<b>%4</b>")
                                .arg(QCoreApplication::applicationName(), QCoreApplication::applicationVersion(), QCoreApplication::organizationName())
                                .arg(m_pServerConfig->getIdentString()));
}

void MainWindow::readSettings()
{
    LOG_INFO("%s, %s", QCoreApplication::organizationName().toStdString().c_str(), QCoreApplication::applicationName().toStdString().c_str());

    Settings settings;
    auto geometry = settings.geometry();

    if (geometry.isEmpty())
    {
        auto size = QSize(800,600);

        auto screens = qApp->screens();
        LOG_INFO("%i screens detected", screens.size());
        if (!screens.empty())
        {
            size = screens.first()->size();
            if (size.width() > 4096)
            {
                size = QSize(1920,1080);
            }
        }

        resize(size.width() * 2/3, size.height() * 2/3);
        move((size.width () - width ()) / 2,
             (size.height() - height()) / 2 );
    }
    else
    {
        restoreGeometry(geometry);
    }
}

void MainWindow::writeSettings()
{
    LOG_INFO("", "");

    Settings settings;
    settings.setGeometry(saveGeometry());
}

} // namespace DCT
