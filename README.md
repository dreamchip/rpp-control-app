# RPP GUI - demo

This project demonstrates the GUI for the RPP demonstrator.

## Prerequisites

* recent installation of QT

## Build

Ensure that Qt5_DIR is configured

```
$ export Qt5_DIR=/data/Qt5.14.0/5.14.0/gcc_64/lib/cmake
```

Create build directory, configure CMake and run the build process

```
$ cd qt-app
$ mkdir debug && cd debug
$ $ cmake ..
-- The C compiler identification is GNU 7.4.0
-- The CXX compiler identification is GNU 7.4.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: ./qt-app/debug

$ make -j12
```
