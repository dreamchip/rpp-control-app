#include <QJsonArray>
#include "Register.h"
#include "Logger.h"


const Register::SpecialMemRegister  Register::m_SpecialMemRegs[] =
{
    // Rehhagel
    { "dpcc_bp_position_reg"    , "dpcc_bp_taddr_reg"        ,  512*4 },
    { "lsc_r_table_data_reg"    , "lsc_r_table_addr_reg"     ,  145*4 },
    { "lsc_gr_table_data_reg"   , "lsc_gr_table_addr_reg"    ,  145*4 },
    { "lsc_b_table_data_reg"    , "lsc_b_table_addr_reg"     ,  145*4 },
    { "lsc_gb_table_data_reg"   , "lsc_gb_table_addr_reg"    ,  145*4 },

    // Hamburg
    { "aeh_bin_data_reg"        , "aeh_bin_addr_reg"         ,  328*4 }, //  520 = 2*2*130
    { "blc_knot_data_reg"       , "blc_knot_addr_reg"        ,  884*4 }, // 1768 = 13*17*4*2/2
    { "blm_result_data_reg"     , "blm_result_addr_reg"      ,   40*4 }, //   40 = 2*16 + 8
    { "blsc_knot_data_reg"      , "blsc_knot_addr_reg"       ,  480*4 }, //  480 = 15*19*2
    { "crtn_candidate_data_reg" , "crtn_candidate_addr_reg"  ,  251*4 }, //  251 = 8032 / 32
    { "dpdc_drb_hist_data_reg"  , "dpdc_drb_hist_addr_reg"   ,  126*4 },
    { "dpdc_pnc_knot_data_reg"  , "dpdc_pnc_knot_addr_reg"   , 1008*4 }, // 1008 = 7*9*8*2
    { "ifrm_pdaf_descr_data_reg", "ifrm_pdaf_descr_adr_reg"  , 4096*4 },
    { "ltm_global_hist_data_reg", "ltm_global_hist_addr_reg" ,  256*4 },
    { "ltm_gmap_bp_lut_data_reg", "ltm_gmap_bp_lut_addr_reg" , 1089*4 }, // 1089 = 33*33
    { "ltm_hist_data_reg"       , "ltm_hist_addr_reg"        ,   64*4 },
    { "ltm_lut_data_reg"        , "ltm_lut_addr_reg"         ,   64*4 },
    { "pdc_gain_knot_data_reg"  , "pdc_gain_knot_addr_reg"   ,  442*4 }, //  442 = 13*17*2
    { "pdc_offset_knot_data_reg", "pdc_offset_knot_addr_reg" ,  442*4 }, //  442 = 13*17*2
    { "pgo_knot_data_reg"       , "pgo_knot_addr_reg"        , 3536*4 }, // 3536 = 13*17*8*2
    { "wdem_cnrne_coef_data_reg", "wdem_cnrne_coef_index_reg",   52*4 }, //   52 = 13*4
    { "wdem_qpc_coef_data_reg"  , "wdem_qpc_coef_index_reg"  ,   72*4 }, //   72 = 8*9
    { "wdem_stg1c_coef_data_reg", "wdem_stg1c_coef_index_reg", 3240*4 }, // 3240 = 27*4*2*5*3
    { "wdem_stg1w_coef_data_reg", "wdem_stg1w_coef_index_reg", 6480*4 }, // 6480 = 27*8*2*5*3

};

////////////////////////////////////////////////////////////////////////////////
// class Access

void Access::setAccess(const QString & mode)
{
    if (mode.isEmpty())
        return;
    QString tmp = mode.toLower();
    if (!tmp.contains(QChar('r')))
    {
        m_mode &= ~AccessRead;
    }
    if (!tmp.contains(QChar('w')))
    {
        m_mode &= ~AccessWrite;
    }
}

////////////////////////////////////////////////////////////////////////////////
// class Slice

Slice::Slice(const QJsonObject& jsonObject, const Access & defAccess, uint32_t index)
  : Access(defAccess)
  , m_CLASS_ID(CLASS_ID_SLICE)
{
    m_Name    = jsonObject["name"       ].toString();
    m_Mask    = jsonObject["mask"       ].toString();
    m_Shift   = jsonObject["shift"      ].toString();
    auto desc = jsonObject["description"].toArray();

    for (const QJsonValue& d : desc)
    {
        m_Descr += d.toString();
        m_Descr += "\n";
    }

    if (index != 0xffffffff)
    {
        m_Name += QString("_%1").arg(index, 4, 10, QChar('0'));
    }

    // check for an access mode specification. if none is given,
    // leave the access mode as given for the parent register.
    setAccess(jsonObject["mode"].toString());
}


////////////////////////////////////////////////////////////////////////////////
// class Register

Register::Register(const QJsonObject& jsonObject, const QString& instanceName, uint32_t baseAddr, uint16_t instanceIdx, uint16_t childIdx, const QString& value)
  : m_pLogger     (new DCT::Logger(""))
  , m_BaseAddr    (baseAddr)
  , m_InstanceIdx (instanceIdx)
  , m_InstanceName(instanceName)
  , m_ChildIdx    (childIdx)
  , m_Length      (0)
  , m_AddrReg     (nullptr)
  , m_changed     (false)
  , m_is_addr_reg (false)
{
    char name[MAX_MODULE_NAME_LEN+1] = { 0 };
    snprintf(name, MAX_MODULE_NAME_LEN, "Reg_%02d_%02d", m_InstanceIdx, m_ChildIdx);
    m_pLogger->setModuleName(name);

    m_Value.append(value);

    fromJsonObj(jsonObject);

    LOG_INFO("name: %-33s, base: 0x%04x, offset: 0x%04x, len: 0x%04x", m_Name.toStdString().c_str(), m_BaseAddr, m_AddrOffset, m_Length);
}

Register::~Register()
{
    //LOG_INFO("", 0);

    delete m_pLogger;
}

void Register::setValue(const QJsonArray& value)
{
    m_Value.clear();

    for (const auto& val: value)
    {
        m_Value.append(val.toString());
    }

    // when initialized JSON data (e.g. read from the simulator, clear the 'modified' flag again.
    m_changed = false;
}

void Register::setValue(const QStringList& value)
{
    // QStringList is implicitly shared, so a direct comparison is possible and direct assignment
    // is save. First, check whether the modified flag shall be set. */
    m_changed |= (value != m_Value);

    // now, remember the new value.
    m_Value = value;
}

void Register::setValue(const QString& value, uint32_t index)
{
    if (index < (uint32_t) m_Value.size())
    {
        // remember whether the value was changed before storing the new value.
        m_changed |= value != m_Value[index];
        m_Value[index] = value;
    }
}

QString Register::getValueAsString()
{
    QString  result;
    bool     first = true;

    for (const auto& val: m_Value)
    {
        if (first)
        {
            first = false;
        }
        else
        {
            result.append(",");
        }

        result.append(val);
    }

    return result;
}

void Register::fromJsonObj(const QJsonObject& jsonObj)
{
    if (jsonObj.find("name") != jsonObj.end())
    {
        m_Name        = jsonObj["name"].toString();
        m_LongName    = QString("%1 %2").arg(m_InstanceName, m_Name);
        auto addr_ofs = jsonObj["address"].toString();
        m_AddrOffset  = addr_ofs.toInt(nullptr, 16);
        m_AddrStr     = QString("0x%1").arg(m_BaseAddr + m_AddrOffset, 8, 16, QChar('0'));
    }

    for (const auto& reg: m_SpecialMemRegs)
    {
        if (m_Name == reg.dataRegName)
        {
            m_Length = reg.length;
            m_AddrRegName = QString("%1 %2").arg(m_InstanceName, reg.addrRegName);
            break;
        }

        if (m_Name == reg.addrRegName)
        {
            m_is_addr_reg = true;
        }
    }

    if (m_Length == 0)
    {
        m_Length = jsonObj["length"].toInt();

        if (m_Length == 0)
        {
            m_Length = 4;
        }
    }

    // Check whether the register is readable and/or writeable. There may be a  "mode" 
    // and/or "writemask" specification. Use the writemask (defaulting  to all bits set) 
    // to determine an initial value for the register  mode (r or rw)  that is used 
    // when the "mode" property does not exist.
    QString wmask = jsonObj["writemask"].toString("0xFFFFFFFF");
    QString defMode = (wmask.toUInt(nullptr, 0)) ? "rw" : "w";
    setAccess(jsonObj["mode"].toString(defMode));

    setValue(jsonObj["value"].toArray());

    if (m_Length <= 4)
    {
        for(const auto& slice_v: jsonObj["slices"].toArray())
        {
            std::shared_ptr<Slice> slice(new Slice(slice_v.toObject(), *this));
            m_SliceList.append(slice);
        }
    }
    else
    {
        const auto& slice_o = jsonObj["slices"].toArray()[0].toObject();
        for (uint32_t i = 0; i < m_Length/4; i++)
        {
            std::shared_ptr<Slice> slice(new Slice(slice_o, *this, i));
            m_SliceList.append(slice);
        }
    }

    // reading from JSON should clear the changed flag.
    m_changed = false;
}

void Register::toJsonObjRead(QJsonObject& jsonObj)
{
    jsonObj.insert("address", m_AddrStr);

    if (isMemoryRegister())
    {
        if (m_AddrReg)
        {
            uint32_t addr = m_AddrReg->address();

            jsonObj.insert("addr_reg_addr", QString("0x%1").arg(addr, 8, 16, QChar('0')));
            jsonObj.insert("addr_reg_value", QString("0"));
        }

        jsonObj.insert("length", QString("0x%1").arg(m_Length, 8, 16, QChar('0')));
        jsonObj.insert("increment", "0");
    }
    else
    {
        jsonObj.insert("length", "4");
    }
}

void Register::toJsonObjWrite(QJsonObject& jsonObj)
{
    QJsonArray valueArray;

    for (const auto& val: m_Value)
    {
        valueArray.append(val);
    }
    auto length = valueArray.size() * 4;

    if (isMemoryRegister())
    {
        if (m_AddrReg)
        {
            uint32_t addr = m_AddrReg->address();

            jsonObj.insert("addr_reg_addr", QString("0x%1").arg(addr, 8, 16, QChar('0')));
            jsonObj.insert("addr_reg_value", QString("0"));
        }
    }

    jsonObj.insert("address", m_AddrStr);
    jsonObj.insert("length", QString("0x%1").arg(length, 8, 16, QChar('0')));

    if (isMemoryRegister())
    {
        jsonObj.insert("increment", "0");
    }

    jsonObj.insert("value", valueArray);
}

void Register::getSliceList(QList<Slice>& list)
{
    for (auto slice : m_SliceList)
    {
        list.append(Slice(*slice));
    }
}

