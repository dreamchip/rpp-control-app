#ifndef _REGISTER_H_
#define _REGISTER_H_

#include <memory>
#include <cstdint>
#include <QString>
#include <QList>
#include <QJsonObject>
#include "ClassIDs.h"


namespace DCT { class Logger; }

class Access
{
    enum AccessMode
    {
        AccessRead      = 1,
        AccessWrite     = 2,
        AccessReadWrite = (AccessRead | AccessWrite),
    };
public:
    Access()
        : m_mode(AccessReadWrite)
    {};
    Access(const Access & that)
        : m_mode(that.m_mode)
    {}
    Access & operator=(const Access & that)
    {
        m_mode = that.m_mode;
        return *this;
    }
    bool isReadable() const
    {
        return (m_mode & AccessRead);
    }
    bool isWriteable() const
    {
        return (m_mode & AccessWrite);
    }
    bool isReadOnly() const
    {
        return m_mode == AccessRead;
    }
    bool isReadWrite() const
    {
        return m_mode == AccessReadWrite;
    }
protected:
    void setAccess(const QString & mode);
private:
    uint8_t     m_mode;
};

class Slice : public Access
{
public:
    Slice(const QJsonObject& jsonObject, const Access & defAccess, uint32_t index = 0xffffffff);
    uint32_t        m_CLASS_ID;
    QString         m_Name;
    QString         m_Mask;
    QString         m_Shift;
    QString         m_Descr;
};

class Register : public Access
{
  public:
    Register(const QJsonObject& jsonObject, const QString& instanceName, uint32_t baseAddr, uint16_t instanceIdx, uint16_t childIdx, const QString& value = QString());

    virtual ~Register();

    void fromJsonObj   (const QJsonObject& jsonObj);
    void toJsonObjRead (      QJsonObject& jsonObj);
    void toJsonObjWrite(      QJsonObject& jsonObj);

    const QStringList& value()  const  { return m_Value; }
    const QString&     name ()  const  { return m_Name ; }
    const QString&  instName()  const  { return m_InstanceName ; }
    const QString&  longName()  const  { return m_LongName; }
    const QString&  addrName()  const  { return m_AddrRegName; }
    uint32_t  baseAddr   ()  const  { return m_BaseAddr   ; }
    uint32_t  address    ()  const  { return m_BaseAddr + m_AddrOffset; }
    uint32_t  addrOffset ()  const  { return m_AddrOffset ; }
    uint32_t  id         ()  const  { return (m_InstanceIdx << 16) + m_ChildIdx; }
    uint16_t  instanceIdx()  const  { return m_InstanceIdx; }
    uint16_t  childIdx   ()  const  { return m_ChildIdx   ; }
    bool      changed    ()  const  { return m_changed    ; }

    void setValue(const QJsonArray& value);
    void setValue(const QStringList& value);
    void setValue(const QString& value, uint32_t index = 0);
    QString getValueAsString();

    void getSliceList(QList<Slice>& list);

    bool isAddressRegister() const  { return m_is_addr_reg; }
    bool isMemoryRegister() const   { return (m_Length > 4);}

    void setAddrReg(Register * reg) { m_AddrReg = reg; }

  protected:
    typedef QList<std::shared_ptr<Slice>> SliceList;
    typedef SliceList::iterator           SliceListIterator;

    struct SpecialMemRegister {
        QString  dataRegName;
        QString  addrRegName;
        uint32_t length;
    };

  protected:
    const uint32_t  m_CLASS_ID = CLASS_ID_REGISTER;
    DCT::Logger*    m_pLogger    ;
    uint32_t        m_BaseAddr   ;
    uint16_t        m_InstanceIdx;
    QString         m_InstanceName;
    uint16_t        m_ChildIdx   ;
    uint32_t        m_AddrOffset { 0 };
    QString         m_AddrStr    { "" };
    QString         m_Name       ;
    QString         m_AddrRegName;
    QString         m_LongName   ;
    uint32_t        m_Length     ;
    QStringList     m_Value      ;
    SliceList       m_SliceList  ;
    Register*       m_AddrReg    ;          // register used for addressing memory
    bool            m_changed    ;
    bool            m_is_addr_reg;          // whether this is an address register for indirect memories.

    static const SpecialMemRegister  m_SpecialMemRegs[];
};

#endif // _REGISTER_H_
