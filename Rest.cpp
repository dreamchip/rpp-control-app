#include <QJsonDocument>
#include "Logger.h"
#include "Rest.h"

#ifdef _DEBUG
#define REPLY_TIMEOUT  3600*1000  // ms
#else
#define REPLY_TIMEOUT  3600*1000  // ms
#endif

Rest::Rest(RestCallback *parent)
  : QObject(parent)
  , m_pLogger(new DCT::Logger("Rest"))
  , m_pCallbackParent(parent)
{
    //LOG_INFO("", 0);

    m_nam = std::make_shared<QNetworkAccessManager>(this);
    connect(m_nam.get(), &QNetworkAccessManager::finished, this, &Rest::Finished);

    connect(this, &Rest::restNotify, parent, &RestCallback::restCallback);
}

Rest::~Rest()
{
    //LOG_INFO("", 0);

    delete m_pLogger;
}

void Rest::enableSsl(bool enable)
{
    m_ssl_enabled = enable;

    //LOG_INFO("", 0);

    delete m_pLogger;
}

void Rest::setupSsl(QNetworkRequest *request)
{
    //LOG_INFO("", 0);

    if (!m_ssl_enabled)
    {
        return;
    }

    QSslConfiguration config = request->sslConfiguration();
    config.setPeerVerifyMode(QSslSocket::VerifyNone);
    config.setProtocol(QSsl::TlsV1_2OrLater);
    request->setSslConfiguration(config);
}

void Rest::Get(const char *host, const int port, const std::string &path)
{
    //LOG_INFO("", 0);

    QJsonDocument json;

    QNetworkRequest request;
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QUrl url;
    url.setScheme(m_ssl_enabled ? "https" : "http");
    url.setHost(host);
    url.setPort(port);
    url.setPath(path.c_str());

    LOG_INFO("%s", url.toString().toStdString().c_str());

    request.setUrl(url);
    setupSsl(&request);

    QNetworkReply* reply = m_nam->get(request);
    ReplyTimeout::set(reply, REPLY_TIMEOUT);
}

void Rest::Post(const char *host, const int port, const std::string &path, const QByteArray &payload)
{
    //LOG_INFO("", 0);

    QNetworkRequest request;
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QUrl url;
    url.setScheme(m_ssl_enabled ? "https" : "http");
    url.setHost(host);
    url.setPort(port);
    url.setPath(path.c_str());
    request.setUrl(url);
    setupSsl(&request);

    QNetworkReply* reply = m_nam->post(request, payload);
    ReplyTimeout::set(reply, REPLY_TIMEOUT);
}

void Rest::Finished(QNetworkReply* reply)
{
    //LOG_INFO("", 0);

    m_nam->disconnect();

    if (m_pCallbackParent)
    {
        emit restNotify(reply);
    }
    else
    {
        if (reply->error() != QNetworkReply::NoError)
        {
            LOG_ERROR("ERR: no callback defined; %s", reply->errorString().toStdString().c_str());
        }
        else
        {
            LOG_ERROR("ERR: no callback defined; %s", reply->readAll().toStdString().c_str());
        }
    }

    reply->deleteLater();
    deleteLater();
}
