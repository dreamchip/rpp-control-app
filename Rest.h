#ifndef REST_H
#define REST_H

#include <memory>
#include <QTimer>
#include <QObject>
#include <QTimerEvent>
#include <QNetworkReply>
#include "ClassIDs.h"


namespace DCT { class Logger; }


const int HTTP_OK = 200;


//
class ReplyTimeout : public QObject {
    Q_OBJECT

  public:
    enum HandleMethod { Abort, Close };

    ReplyTimeout(QNetworkReply* reply, const int timeout, HandleMethod method = Close)
      : QObject(reply)
      , m_method(method)
    {
        Q_ASSERT(reply);
        if (reply && reply->isRunning())
        {
            m_timer.start(timeout, this);
            connect(reply, &QNetworkReply::finished, this, &QObject::deleteLater);
        }
    }

    static void set(QNetworkReply* reply, const int timeout, HandleMethod method = Close)
    {
        new ReplyTimeout(reply, timeout, method);
    }

  protected:
    void timerEvent(QTimerEvent * ev)
    {
        if (!m_timer.isActive() || ev->timerId() != m_timer.timerId())
        {
            return;
        }

        auto reply = static_cast<QNetworkReply*>(parent());
        if (reply->isRunning())
        {
            if (m_method == Close)
            {
                reply->close();
            }
            else if (m_method == Abort)
            {
                reply->abort();
            }
            m_timer.stop();
        }
    }

  protected:
    QBasicTimer     m_timer;
    HandleMethod    m_method;
};

class RestCallback : public QObject
{
    Q_OBJECT
  public:
    RestCallback(QObject* parent) : QObject(parent) {}
    virtual ~RestCallback() {}
  public slots:
    virtual void restCallback(QNetworkReply* reply) = 0;
};

//
class Rest : public QObject
{
    Q_OBJECT
  public:
    explicit Rest(RestCallback* parent = nullptr);
    virtual ~Rest();

    void enableSsl(bool enable);
    void Get(const char *host, const int port, const std::string &path);
    void Post(const char *host, const int port, const std::string &path, const QByteArray &payload);

  signals:
    void restNotify(QNetworkReply* reply);

  public slots:
    void Finished(QNetworkReply* reply);

  private:
    const uint32_t  m_CLASS_ID = CLASS_ID_REST;
    DCT::Logger*    m_pLogger;
    RestCallback*   m_pCallbackParent;
    std::shared_ptr<QNetworkAccessManager>  m_nam;
    bool            m_ssl_enabled = false;

    void setupSsl(QNetworkRequest *request);
};

#endif // REST_H
