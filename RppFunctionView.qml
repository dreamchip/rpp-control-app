import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQml.Models 2.2
import QtQuick.Extras 1.4
import application 1.0

Item {
    id: element
    property alias functionTable: functionTable
    property alias parameterTable: parameterTable
    property alias blockTable: blockTable
    property alias textDescription: textDescription
    property alias btnFunction: btnFunction
    property alias statusIndicator: statusIndicator
    property alias functionName: functionName
    property alias tableViewColumn: tableViewColumn

    ColumnLayout {
        id: columnLayout
        width: parent.width
        height: parent.height - 40
        x: 0
        y: 20

        RowLayout {
            ComboBox {
                id: blockTable
                Layout.fillWidth: true
                textRole: "type"
                model: display.block_list
                onCurrentIndexChanged: {
                    display.onBlockIndexChanged(blockTable.currentIndex);
                }
            }
        }

        RowLayout {
            TableView {
                id: functionTable
                height: 250
                Layout.fillHeight: true
                Layout.fillWidth: true
                model: display.function_table
                onClicked: {
                    display.onClickedFunction(row);
                }
                TableViewColumn {
                    role: "role0"
                    title: qsTr("Function")
                }
            }
            TableView {
                id: parameterTable
                height: 250
                Layout.fillHeight: true
                Layout.fillWidth: true
                model: display.parameter_table
                onClicked: {
                    display.onClickedParameter(row);
                }
                TableViewColumn {
                    role: "role0"
                    title: qsTr("Parameter")
                }
                TableViewColumn {
                    role: "role1"
                    title: qsTr("Mode")
                    width: 60
                }
                TableViewColumn {
                    role: "role2"
                    title: qsTr("Type")
                    width: 60
                }
                TableViewColumn {
                    id: tableViewColumn
                    role: "role3"
                    title: qsTr("Value")
                    delegate: TextField {
                        text: styleData.value
                        placeholderText: "value"
                        onTextChanged: {
                            display.parameter_table.setData(styleData.row, text, "role3");
                        }
                    }
                }
            }
        }

        RowLayout {
            TextArea {
                id: textDescription
                Layout.fillWidth: true
                text: display.function_description
            }
        }
        RowLayout {
            Label {
                id: functionName
                text: display.function_name
                horizontalAlignment: Text.AlignLeft
                Layout.minimumWidth: 150
            }

            Button {
                id: btnFunction
                text: qsTr("Run")
                onClicked: {
                    display.onRunFunction();
                }
            }

            StatusIndicator {
                id: statusIndicator
                color: display.status_color
                active: display.status_active
                Layout.preferredHeight: 20
                Layout.preferredWidth: 20
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
