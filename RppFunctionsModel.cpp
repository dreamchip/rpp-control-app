#include <QDebug>
#include <QJsonObject>
#include <QVariant>
#include <QFile>
#include <QSettings>
#include <QCoreApplication>
#include <sstream>
#include "RppFunctionsModel.h"
#include "Logger.h"
#include "ClassIDs.h"
#include "Rest.h"
#include "Schema.h"
#include "Function.h"
#include "Settings.h"

#if !WIN32
#include <unistd.h>
#endif


#define FUNC_NAME_START_PROCESSING  tr("Process")
#define FUNC_NAME_CHECK_STATUS      tr("cfg_status")

//
RppFunctionsModel::RppFunctionsModel(QObject* parent)
  : RestCallback(parent)
  , m_pLogger        (new DCT::Logger("App"))
  , m_block_list     (new ListModel       (this, "Blocks"    ))
  , m_function_table (new TableModel   (1, this, "Functions" ))
  , m_parameter_table(new TableModel   (4, this, "Parameters"))
{
    LOG_INFO("", 0);

    reset();

    LOG_INFO("target: address: %s, port: %d", m_board_address.c_str(), m_port);
}

//
RppFunctionsModel::~RppFunctionsModel()
{
    stopCheckStatusTimer();

    m_function_list.clear();

    delete m_parameter_table;
    delete m_function_table;
    delete m_block_list;

    LOG_INFO("", 0);

    delete m_pLogger;
}

//
void RppFunctionsModel::reset()
{
    stopCheckStatusTimer();

    m_function_list.clear();
    m_block_list     ->clean();
    m_function_table ->clean();
    m_parameter_table->clean();

    Settings settings;
    m_board_address = settings.targetIpAddress().toStdString();
    m_port          = settings.targetIpPort   ().toInt();
}

//
void RppFunctionsModel::fillFunctionModels(QString& val)
{
    LOG_INFO("", 0);

    reset();

    QJsonDocument jsdoc = QJsonDocument::fromJson(val.toUtf8());
    m_function_json = jsdoc.array();

    uint16_t block_num = 0;

    // loop over all function blocks
    for (const QJsonValue& value : m_function_json)
    {
        auto block = value.toObject();
        auto block_name = block["name"].toString();
        auto data = block["function"].toArray();

        m_block_list->append(block_name, "");

        if (data.isEmpty())
        {
            continue;
        }

        uint16_t func_num = 0;

        // loop over all functions in function block
        for (const auto& f : data)
        {
            std::shared_ptr<Function> func(new Function(f.toObject(), block_name, block_num, func_num));
            m_function_list.append(func);

            LOG_INFO("%s", func->funcName().toStdString().c_str());

            func_num++;
        }

        block_num++;
    }

    startCheckStatusTimer();

    for (auto pFunc: m_function_list)
    {
        if (pFunc->hasRWParams())
        {
            m_get_params_list.append(pFunc);
        }
    }

    handleGetParameters(); // start reading registers

    onBlockIndexChanged(0);
}

//
void RppFunctionsModel::loadFunctionsFromDisk(QString filename)
{
    LOG_INFO("%s", filename.toStdString().c_str());

    // read file content to string
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        LOG_WARN("can't open file %s", filename.toStdString().c_str());
        return;
    }
    QByteArray data = file.readAll();
    file.close();

    if (data.length() == 0)
    {
        LOG_WARN("data.length() == 0", 0);
    }

    auto  jsdoc  = QJsonDocument::fromJson(data);
    auto  func_a = jsdoc.array();

    if (func_a.size() == 0)
    {
        LOG_WARN("func_a.size() == 0", 0);
    }

    // extract from JSON and write registers

    m_set_params_list.clear();

    for (const auto& f : func_a)
    {
        auto func      = f.toObject();
        auto func_name = func["name"].toString();
        auto param_a   = func["parameter"].toArray();
        auto found     = false;

        for (auto pFunction: m_function_list)
        {
            if (pFunction->funcName() == func_name)
            {
                for (const auto& param : param_a)
                {
                    found = true;
                    pFunction->updateParameter(param.toObject());
                }

                m_set_params_list.append(pFunction);
            }
        }

        if (!found)
        {
            LOG_ERROR("function %s not found", func_name.toStdString().c_str());
        }
    }

    handleSetParameters(); // start calling functions
}

//
void RppFunctionsModel::handleSetParameters()
{
    if (!m_set_params_list.empty())
    {
        const auto& func = m_set_params_list.front();

        callFunction(func, true);
        m_set_params_list.pop_front();
    }
}

//
void RppFunctionsModel::saveFunctionsToDisk(QString filename)
{
    LOG_INFO("%s", filename.toStdString().c_str());

    m_save_functions_file.setFileName(filename);
    if (!m_save_functions_file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        LOG_WARN("can't open file %s", filename.toStdString().c_str());
        return;
    }

    //m_read_functions_list.clear();

    for (auto pFunc: m_function_list)
    {
        if (pFunc->hasRWParams())
        {
            m_get_params_list.append(pFunc);
        }
    }

    handleGetParameters(); // start reading registers
}

//
void RppFunctionsModel::handleGetParameters()
{
    if (!m_get_params_list.empty())
    {
        const auto& func = m_get_params_list.front();
        callFunction(func, false);
        m_get_params_list.pop_front();
    }
    else if (m_save_functions_file.isOpen())
    {
        QJsonArray   jsonArray;

        for (auto pFunc: m_function_list)
        {
            if (pFunc->hasRWParams())
            {
                QJsonObject func;
                pFunc->toJsonObj(func);
                jsonArray.append(func);
            }
        }

        QByteArray data = QJsonDocument(jsonArray).toJson();
        m_save_functions_file.write(data);
        m_save_functions_file.close();
    }
}

//
void RppFunctionsModel::setDescription(QString description)
{
    LOG_INFO("", 0);

    if (m_curr_description != description)
    {
        m_curr_description = description;
        emit descriptionChanged();
    }
}

//
void RppFunctionsModel::setActiveStatus(bool status)
{
    if (m_status_active != status)
    {
        m_status_active = status;
        emit statusActiveChanged();
    }
}

//
void RppFunctionsModel::setStatusColor(QColor color)
{
    if (m_status_color != color)
    {
        m_status_color = color;
        emit statusColorChanged();
    }
}

//
void RppFunctionsModel::setStatusText(const QString& text)
{
    if (m_status_text != text)
    {
        m_status_text = text;
        emit statusTextChanged();
    }
}

//
void RppFunctionsModel::setFunctionName(QString name)
{
    if (m_curr_function_name != name)
    {
        m_curr_function_name = name;
        emit functionNameChanged();
    }
}

// updates parameter list when new function has been selected
void RppFunctionsModel::onClickedFunction(const int& row)
{
    LOG_INFO("", 0);

    auto index = m_function_table->index(row, 0);
    auto name  = m_function_table->data(index, ROLE_FUNC_NAME).toString();

    for (const auto& pFunction : m_function_list)
    {
        if (pFunction->funcName() == name)
        {
            setFunctionName(name);

            m_parameter_table->clean();

            QList<QStringList> list;
            pFunction->getParamList(list);
            for (const auto& stringList: list)
            {
                m_parameter_table->append(stringList);
            }

            m_parameter_description.clear();
            pFunction->getParamDescr(m_parameter_description);

            setDescription(pFunction->description());

            return;
        }
    }

    LOG_ERROR("Function %s not found", name.toStdString().c_str());
}

//
void RppFunctionsModel::onClickedParameter(const int& index)
{
    LOG_INFO("", 0);

    if (m_parameter_description.empty() || index >= m_parameter_description.count())
    {
        setDescription("");
    }
    else
    {
        setDescription(m_parameter_description[index]);
    }
}

//
void RppFunctionsModel::onBlockIndexChanged(const int& index)
{
    LOG_INFO("", 0);

    if (m_function_json.empty())
    {
        return;
    }

    QJsonValue  value = m_function_json[index];
    QJsonObject block = value.toObject();
    QString     name  = block["name"].toString();
    QJsonArray  data  = block["function"].toArray();

    m_function_table->clean();

    if (data.isEmpty())
    {
        return;
    }

    //uint32_t child_num = 0;

    for (const QJsonValue& v : data)
    {
        QJsonObject func = v.toObject();
        QStringList val;

        val << func["name"].toString();
        m_function_table->append(val);
        //child_num++;
    }

    onClickedFunction(0);
}

//
void RppFunctionsModel::startCheckStatusTimer()
{
    //m_check_status_timer.start(1000, this);
}

//
void RppFunctionsModel::stopCheckStatusTimer()
{
    m_check_status_timer.stop();
}

void RppFunctionsModel::timerEvent(QTimerEvent* event)
{
    if (event->timerId() == m_check_status_timer.timerId())
    {
        checkStatus();
    }
}

//
void RppFunctionsModel::checkStatus()
{
    QJsonArray  para_array;

    for (const auto& pFunction : m_function_list)
    {
        if (pFunction->funcName() == FUNC_NAME_CHECK_STATUS)
        {
            QList<QStringList>  list;
            pFunction->getParamList(list);
            for (const auto& stringList: list)
            {
                QJsonObject para_data;
                para_data.insert("name" , stringList[0]);
                para_data.insert("mode" , stringList[1]);
                para_data.insert("type" , stringList[2]);
                //para_data.insert("value", stringList[3]);
                para_array.append(para_data);
            }

            callFunction(FUNC_NAME_CHECK_STATUS, para_array);

            return;
        }
    }

    LOG_ERROR("Function %s not found", FUNC_NAME_CHECK_STATUS.toStdString().c_str());
}

//
void RppFunctionsModel::startProcessing()
{
    callFunction(FUNC_NAME_START_PROCESSING);
}

//
void RppFunctionsModel::onRunFunction()
{
    func(m_curr_function_name);

    setActiveStatus(true);
    setStatusColor("yellow");
}

//
void RppFunctionsModel::func(const QString& name)
{
    LOG_INFO("", 0);

    QJsonArray  para_array;

    for (int row = 0; row < m_parameter_table->rowCount(QModelIndex()); ++row)
    {
        QJsonObject para_data;
        para_data.insert("name" , m_parameter_table->data(m_parameter_table->index(row, 0, QModelIndex()), ROLE_PARAM_NAME ).toString());
        para_data.insert("mode" , m_parameter_table->data(m_parameter_table->index(row, 0, QModelIndex()), ROLE_PARAM_MODE ).toString());
        para_data.insert("type" , m_parameter_table->data(m_parameter_table->index(row, 0, QModelIndex()), ROLE_PARAM_TYPE ).toString());
        para_data.insert("value", m_parameter_table->data(m_parameter_table->index(row, 0, QModelIndex()), ROLE_PARAM_VALUE).toString());
        para_array.append(para_data);
    }

    callFunction(name, para_array);

    setActiveStatus(true);
    setStatusColor("yellow");
}

void RppFunctionsModel::callFunction(const QString& name, const QJsonArray& parameters)
{
    //LOG_INFO("", 0);

    QJsonObject data;
    QJsonArray  func_array;
    QJsonObject func;
    QJsonObject payload;

    func.insert("name", name);
    func.insert("parameter", parameters);
    func_array.append(func);
    payload.insert("func", func_array);
    data.insert("data", payload);
    QJsonDocument doc(data);

    if (name != FUNC_NAME_CHECK_STATUS)
    {
        LOG_INFO("%s", doc.toJson().toStdString().c_str());
    }

    Rest* r = new Rest(this);
    r->Post(m_board_address.c_str(), m_port, m_cmd_func, doc.toJson());
}

void RppFunctionsModel::callFunction(std::shared_ptr<Function> function, bool inclValues)
{
    //LOG_INFO("", 0);

    QJsonObject data;
    QJsonObject payload;
    QJsonArray  func_array;
    QJsonObject func;

    function->toJsonObj(func, inclValues);

    func_array.append(func);
    payload   .insert("func", func_array);
    data      .insert("data", payload);
    QJsonDocument doc(data);

    LOG_INFO("%s", doc.toJson().toStdString().c_str());

    Rest* r = new Rest(this);
    r->Post(m_board_address.c_str(), m_port, m_cmd_func, doc.toJson());
}

//
QJsonDocument RppFunctionsModel::Finished(QNetworkReply* reply)
{
    //LOG_INFO("", 0);

    QJsonDocument json;

    auto error = reply->error();

    if (error != QNetworkReply::NoError)
    {
        LOG_ERROR("reply->error(): %i - %s", error, reply->errorString().toStdString().c_str());
        //addMessageLogEntry("error", reply->errorString());
    }
    else
    {
        auto status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if (status != HTTP_OK)
        {
            LOG_ERROR("reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(): %i", status);
            //addMessageLogEntry("error", reply->errorString());
        }
        else
        {
            json = QJsonDocument::fromJson(reply->readAll());
        }
    }

    reply->deleteLater();

    return json;
}

//
void RppFunctionsModel::restCallback(QNetworkReply* reply)
{
    //LOG_INFO("", 0);

    QJsonDocument jsdoc = Finished(reply);
    QJsonObject   jsobj = jsdoc.object();
    QJsonObject   data  = jsobj["data"].toObject();

    if (data.isEmpty())
    {
        LOG_WARN("reply is empty", "");
        setStatusColor("red");
        return;
    }

    //LOG_INFO("%s", jsdoc.toJson().toStdString().c_str());

    int         error      = data["error"].toInt();
    QJsonArray  func_array = data["func" ].toArray();

    if (error)
    {
        setStatusColor("red");
        QString info = data["info"].toString();
        LOG_ERROR("%s", info.toStdString().c_str());
    }
    else
    {
        setStatusColor("lightsteelblue");
    }

    if (!func_array.isEmpty())
    {
        for (const QJsonValue& v : func_array)
        {
            auto func    = v.toObject();
            auto name    = func["name"].toString();
            auto param_a = func["parameter"].toArray();

            for (const auto& pFunction : m_function_list)
            {
                if (pFunction->funcName() == name)
                {
                    for (const auto& param : param_a)
                    {
                        pFunction->updateParameter(param.toObject());
                    }

                    if (pFunction->funcName() == FUNC_NAME_CHECK_STATUS)
                    {
                        QList<QStringList>  list;
                        pFunction->getParamList(list);
                        QString string = QString("Status: %1, Parameterized: %2, Runtime: %3").arg(list[0][3], list[1][3], list[2][3]);
                        setStatusText(string);
                    }
                    else
                    {
                        LOG_INFO("func %s", name.toStdString().c_str());
                        LOG_INFO("%s", jsdoc.toJson().toStdString().c_str());
                    }

                    if (pFunction->funcName() == m_curr_function_name)
                    {
                        m_parameter_table->clean();
                        QList<QStringList>  list;
                        pFunction->getParamList(list);
                        for (const auto& stringList: list)
                        {
                            m_parameter_table->append(stringList);
                        }
                    }

                    break;
                }
            }
        }
    }

    handleSetParameters();
    handleGetParameters();
}
