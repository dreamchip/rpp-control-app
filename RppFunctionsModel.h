#ifndef RPP_FUNCTIONS_MODEL_H
#define RPP_FUNCTIONS_MODEL_H

#include <memory>
#include <QObject>
#include <QTime>
#include <QTimer>
#include <QColor>
#include <QFile>

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QSslConfiguration>
#include <QUrl>
#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonArray>

#include "ClassIDs.h"
#include "ListModel.h"
#include "TableModel.h"
#include "Rest.h"


namespace DCT { class Logger; }
class Function;


//
class RppFunctionsModel : public RestCallback
{
    Q_OBJECT

    Q_PROPERTY(bool        status_active    READ getStatusActive    NOTIFY statusActiveChanged)
    Q_PROPERTY(QColor      status_color     READ getStatusColor     NOTIFY statusColorChanged)

    Q_PROPERTY(ListModel*  block_list       READ getBlockList       CONSTANT)
    Q_PROPERTY(TableModel* function_table   READ getFunctionTable   CONSTANT)
    Q_PROPERTY(TableModel* parameter_table  READ getParameterTable  CONSTANT)
    Q_PROPERTY(QString     function_name    READ getFunctionName    NOTIFY functionNameChanged)
    Q_PROPERTY(QString     description      READ getDescription     NOTIFY descriptionChanged)
    Q_PROPERTY(QString     status_text      READ getStatusText      NOTIFY statusTextChanged)

  public:
    RppFunctionsModel(QObject* parent = nullptr);
    ~RppFunctionsModel();
    void reset();

    void fillFunctionModels(QString& val);
    void loadFunctionsFromDisk(QString urlString);
    void saveFunctionsToDisk  (QString urlString);

    ListModel*  getBlockList     () const  { return m_block_list;      }
    TableModel* getFunctionTable () const  { return m_function_table;  }
    TableModel* getParameterTable() const  { return m_parameter_table; }

    Q_INVOKABLE void onBlockIndexChanged(const int& index);
    Q_INVOKABLE void onClickedFunction  (const int& index);
    Q_INVOKABLE void onClickedParameter (const int& index);
    Q_INVOKABLE void onRunFunction      ();

  public slots:
    QString getDescription () const  { return m_curr_description; }
    bool    getStatusActive() const  { return m_status_active; }
    QColor  getStatusColor () const  { return m_status_color; }
    QString getFunctionName() const  { return m_curr_function_name; }
    QString getStatusText  () const  { return m_status_text; }
    void    startProcessing();

  signals:
    void descriptionChanged ();
    void statusActiveChanged();
    void statusColorChanged ();
    void functionNameChanged();
    void statusTextChanged();

  private slots:
    void restCallback(QNetworkReply* reply) override;

  private:
    typedef QVector<std::shared_ptr<Function>>  FunctionList;

    void setDescription    (QString description);
    void setStatusColor    (QColor color);
    void setActiveStatus   (bool status);
    void setFunctionName   (QString name);
    void setStatusText     (const QString& text);
    void checkStatus       ();
    void func              (const QString& name);
    void callFunction      (const QString& name, const QJsonArray& parameters = QJsonArray());
    void callFunction      (std::shared_ptr<Function> function, bool inclValues = true);
    QJsonDocument  Finished(QNetworkReply* reply);
    void startCheckStatusTimer();
    void stopCheckStatusTimer();
    void timerEvent(QTimerEvent* event) override;
    void handleSetParameters();
    void handleGetParameters();

    enum
    {
        ROLE_FUNC_NAME   = TableModel::Role0,

        ROLE_PARAM_NAME  = TableModel::Role0,
        ROLE_PARAM_MODE  = TableModel::Role1,
        ROLE_PARAM_TYPE  = TableModel::Role2,
        ROLE_PARAM_VALUE = TableModel::Role3,
    };

    const uint32_t      m_CLASS_ID = CLASS_ID_RPP_FUNCTION_MODEL;
    DCT::Logger*        m_pLogger;

    QJsonArray          m_function_json;
    ListModel*          m_block_list;
    FunctionList        m_function_list;
    TableModel*         m_function_table;
    TableModel*         m_parameter_table;
    QString             m_curr_function_name;
    QStringList         m_parameter_description;
    QString             m_curr_description;
    std::string         m_board_address;
    int                 m_port;
    const char*         m_cmd_func      = "/v1/rpp/func";
    bool                m_status_active = false;
    QColor              m_status_color  = "red";
    QString             m_status_text   = "";
    QBasicTimer         m_check_status_timer;
    QFile               m_save_functions_file;
    FunctionList        m_set_params_list;
    FunctionList        m_get_params_list;
};

#endif // RPP_FUNCTIONS_MODEL_H
