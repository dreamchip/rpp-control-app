import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQml.Models 2.2
import QtQuick.Extras 1.4

Item {
    id: element
    property alias functionTable: functionTable
    property alias parameterTable: parameterTable
    property alias blockTable: blockTable
    property alias textDescription: textDescription
    property alias btnFunction: btnFunction
    property alias functionName: functionName
    property alias statusIndicator: statusIndicator
    property alias tableViewColumn: tableViewColumn

    ColumnLayout {
        id: columnLayout
        width: parent.width
        height: parent.height - 5

        RowLayout {
            ComboBox {
                id: blockTable
                Layout.fillWidth: true
                textRole: "type"
                model: funcModel.block_list
                onCurrentIndexChanged: {
                    funcModel.onBlockIndexChanged(blockTable.currentIndex);
                }
            }
        }

        RowLayout {
            TableView {
                id: functionTable
                height: 250
                Layout.minimumWidth: 100
                Layout.preferredWidth: 150
                Layout.maximumWidth: 200
                Layout.fillWidth: true
                Layout.fillHeight: true
                model: funcModel.function_table
                onClicked: {
                    funcModel.onClickedFunction(row);
                }
                TableViewColumn {
                    role: "role0"
                    title: qsTr("Function")
                }
            }
            TableView {
                id: parameterTable
                height: 250
                width: 300
                Layout.minimumWidth: 150
                Layout.preferredWidth: 350
                Layout.fillHeight: true
                Layout.fillWidth: true
                model: funcModel.parameter_table
                onClicked: {
                    funcModel.onClickedParameter(row);
                }
                TableViewColumn {
                    role: "role0"
                    title: qsTr("Parameter")
                    width: 100
                }
                TableViewColumn {
                    role: "role1"
                    title: qsTr("Mode")
                    width: 50
                }
                TableViewColumn {
                    role: "role2"
                    title: qsTr("Type")
                    width: 70
                }
                TableViewColumn {
                    id: tableViewColumn
                    role: "role3"
                    title: qsTr("Value")
                    width: 100
                    delegate: TextField {
                        text: styleData.value
                        placeholderText: "value"
                        onTextChanged: {
                            if (activeFocus)
                            {
                                funcModel.parameter_table.setData(styleData.row, text, "role3");
                            }
                        }
                    }
                }
            }
        }

        RowLayout {
            TextArea {
                id: textDescription
                Layout.fillWidth: true
                text: funcModel.description
            }
        }
        RowLayout {
            Button {
                id: btnFunction
                text: qsTr("Run")
                onClicked: {
                    funcModel.onRunFunction();
                }
            }

            Label {
                id: functionName
                text: funcModel.function_name
                horizontalAlignment: Text.AlignLeft
                Layout.minimumWidth: 150
            }

            StatusIndicator {
                id: statusIndicator
                color: funcModel.status_color
                active: funcModel.status_active
                Layout.preferredHeight: 20
                Layout.preferredWidth: 40
            }

            Label {
                id: status
                text: funcModel.status_text
                horizontalAlignment: Text.AlignLeft
                Layout.minimumWidth: 200
            }
        }
    }
}
