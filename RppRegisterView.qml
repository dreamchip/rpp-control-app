import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQml.Models 2.2
import QtQuick.Extras 1.4
import application 1.0

Item {
    id: element
    property alias registerTable: registerTable
    property alias sliceTable: sliceTable
    property alias instanceTable: instanceTable
    property alias textDescription: textDescription
    property alias btnWrite: btnWrite
    property alias btnRead: btnRead
    property alias statusIndicator: statusIndicator

    ColumnLayout {
        id: columnLayout
        width: parent.width
        height: parent.height - 40
        x: 0
        y: 20

        RowLayout {
            ComboBox {
                id: instanceTable
                Layout.fillWidth: true
                textRole: "type"
                model: display.instance_list
                onCurrentIndexChanged: {
                    display.onInstanceIndexChanged(instanceTable.currentIndex);
                }
            }
        }

        RowLayout {
            TableView {
                id: registerTable
                Layout.fillWidth: true
                Layout.fillHeight: true
                model: display.register_table
                TableViewColumn {
                    role: "role0"
                    title: qsTr("Register")
                }
                TableViewColumn {
                    role: "role1"
                    title: qsTr("Address")
                }
                TableViewColumn {
                    role: "role2"
                    title: qsTr("Value")
                    delegate: TextField {
                        text: styleData.value
                        placeholderText: "value"
                        onActiveFocusChanged: {
                            if (activeFocus)
                            {
                                console.log("text field")
                                display.onClickedRegister(styleData.row);
                            }
                        }
                        onTextChanged: {
                            console.log("text field")
                            display.register_table.setData(styleData.row, text, "role2");
                        }
                    }
                }
                onClicked: {
                    console.log("table view")
                    display.onClickedRegister(row);
                }
                onActiveFocusChanged: {
                    if (activeFocus)
                    {
                        console.log("table view")
                        display.onClickedRegister(row);
                    }
                }
                onFocusChanged: {
                    if (focus)
                    {
                        console.log("table view")
                        display.onClickedRegister(row);
                    }
                }
            }
            TableView {
                id: sliceTable
                Layout.fillWidth: true
                Layout.fillHeight: true
                model: display.slice_table
                TableViewColumn {
                    role: "role0"
                    title: qsTr("Slice")
                }
                TableViewColumn {
                    role: "role1"
                    title: qsTr("Mask")
                    width: 80
                }
                TableViewColumn {
                    role: "role2"
                    title: qsTr("Shift")
                    width: 80
                }
                TableViewColumn {
                    role: "role3"
                    title: qsTr("Value")
                }
                onClicked: {
                    display.onClickedSlice(row);
                }
            }
        }

        RowLayout {
            TextArea {
                id: textDescription
                Layout.fillWidth: true
                text: display.description
            }
        }
        RowLayout {

            Button {
                id: btnWrite
                text: qsTr("Write")
                onClicked: {
                    display.onClickedRegisterWrite();
                }
            }

            Button {
                id: btnRead
                text: qsTr("Read")
                onClicked: {
                    display.onClickedRegisterRead();
                }
            }

            StatusIndicator {
                id: statusIndicator
                color: display.status_color
                active: display.status_active
                Layout.preferredHeight: 20
                Layout.preferredWidth: 20
            }
        }
    }
}
/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

