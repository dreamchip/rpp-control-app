#include <QDebug>
#include <QJsonObject>
#include <QVariant>
#include <QFile>
#include <QCoreApplication>
#include <sstream>
#include "RppRegistersModel.h"
#include "Logger.h"
#include "ClassIDs.h"
#include "Rest.h"
#include "Schema.h"
#include "Instance.h"
#include "Register.h"
#include "Function.h"
#include "Settings.h"

#if !WIN32
#include <unistd.h>
#endif


const RppRegistersModel::HistogramIDs  RppRegistersModel::ALL_HISTOGRAM_IDS[] = { HISTOGRAM_ID_PRE1, HISTOGRAM_ID_PRE2, HISTOGRAM_ID_PRE3, HISTOGRAM_ID_POST };


//
RppRegistersModel::RppRegistersModel(QObject* parent)
  : RestCallback      (parent)
  , m_pLogger         (new DCT::Logger("App"))
  , m_instance_list   (new ListModel       (this, "Instances" ))
  , m_register_table  (new TableModel   (4, this, "Registers" ))
  , m_slice_table     (new TableModel   (4, this, "Slices"    ))
  , m_req_active      (false)
{
    LOG_INFO("", 0);

    reset();

    LOG_INFO("target: address: %s, port: %d", m_board_address.c_str(), m_port);
}

//
RppRegistersModel::~RppRegistersModel()
{
    StopCurrentTasks();

    m_instance_map.clear();
    m_register_map.clear();

    delete m_slice_table;
    delete m_register_table;
    delete m_instance_list;

    LOG_INFO("", 0);

    delete m_pLogger;
}

//
void RppRegistersModel::reset()
{
    StopCurrentTasks();
    m_instance_map.clear();
    m_register_map.clear();
    m_instance_list ->clean();
    m_register_table->clean();
    m_slice_table   ->clean();
    m_curr_instance_index = 0;
    m_curr_register_index = -1;

    Settings settings;
    m_board_address = settings.targetIpAddress().toStdString();
    m_port          = settings.targetIpPort   ().toInt();
}

//
void RppRegistersModel::fillRegisterModels(QString& val)
{
    LOG_INFO("", 0);

    reset();

    for (int i = 0; i < NUM_HISTOGRAM_IDS; i++)
    {
        m_HistogramRegList[i].clear();
    }

    auto     jsdoc    = QJsonDocument::fromJson(val.toUtf8());
    uint16_t inst_num = 0;
    int      num_histograms = 0;

    m_register_json = jsdoc.array();

    // Loop over all instances within JSON file
    for (const auto& inst_value : m_register_json)
    {
        auto inst_obj   = inst_value.toObject();
        auto file       = inst_obj["file"    ].toString();
        auto name       = inst_obj["name"    ].toString();
        auto offset_str = inst_obj["offset"  ].toString();
        auto registers  = inst_obj["register"].toArray();
        auto offset     = offset_str.toUInt(nullptr, 16);
        int  hist_id    = -1;

        SharedInstPtr pInst(new Instance(file, name, offset, inst_num));
        m_instance_map.insert(pInst->offset(), pInst);

        m_instance_list->append(name + " : " + offset_str, "");

        if (file == "rpp_hist_regs")
        {
            if (num_histograms < NUM_HISTOGRAM_IDS)
            {
                hist_id = num_histograms;
                num_histograms++;
            }
            else
            {
                LOG_ERROR("histogram_id: %d >= NUM_HISTOGRAM_IDS", hist_id);
            }
        }

        if (registers.isEmpty())
        {
            continue;
        }

        uint16_t child_num = 0;

        // Loop over all registers within single instance section
        for (const auto& reg : registers)
        {
            SharedRegPtr pReg(new Register(reg.toObject(), name, offset, inst_num, child_num));
            m_register_map.insert(pReg->address(), pReg);

            if ( (hist_id != -1) && (pReg->name().startsWith("hist_bin_")) )
            {
                m_HistogramRegList[hist_id].append(pReg);
            }

            child_num++;
        }

        inst_num++;
    }

    // Loop over all registers again and assign indirect RAM Address
    // Registers to the indirect RAM Data Registers.
    for (auto& pRegister : m_register_map)
    {
        if (pRegister->isMemoryRegister())
        {
            auto pAddrReg = findRegByLongName(pRegister->addrName());
            if (pAddrReg)
            {
                pRegister->setAddrReg(pAddrReg.get());
            }
        }
    }

    onInstanceIndexChanged(0);
}

//
void RppRegistersModel::saveRegistersToDisk(QString filename, bool all)
{
    LOG_INFO("%s", filename.toStdString().c_str());

    m_save_regs_file.setFileName(filename);
    if (!m_save_regs_file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        LOG_WARN("can't open file %s", filename.toStdString().c_str());
        return;
    }

    m_save_regs_all = all;

    for (auto pRegister : m_register_map)
    {
        // Refresh if the 'all' flag is not given, just refresh all r/w registers.
        if (pRegister && (all || pRegister->isReadWrite()))
        {
            m_read_regs_list.append(pRegister);
        }
    }

    // Submit requests for reading all registers.
    handleReadRegisters();

    // Submit an empty read request to get the data saved.
    addRequest(m_cmd_readl, QString());
}

//
void RppRegistersModel::loadRegistersFromDisk(QString filename, int addrOffset)
{
    LOG_INFO("%s", filename.toStdString().c_str());

    // Read file content to string
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        LOG_WARN("can't open file %s", filename.toStdString().c_str());
        return;
    }
    QByteArray data = file.readAll();
    file.close();

    if (data.length() == 0)
    {
        LOG_WARN("data.length() == 0", 0);
    }

    auto  jsdoc = QJsonDocument::fromJson(data);
    auto  reg_a = jsdoc.array();

    if (reg_a.size() == 0)
    {
        LOG_WARN("reg_a.size() == 0", 0);
        return;
    }

    // Extract from JSON and write registers handle indirect RAM data registers specially.
    m_write_regs_list.clear();

    QJsonArray          out_regs;
    QList<Register*>   mems_to_read;

    for (const auto& inst_value : reg_a)
    {
        auto  reg_obj = inst_value.toObject();
        auto  addr_str = reg_obj["address"].toString();
        auto  name = reg_obj["name"].toString();
        auto  val_a = reg_obj["value"].toArray();
        auto  addr = addr_str.toInt(nullptr, 16);

        SharedRegPtr pRegister;

        if (addr + addrOffset < 0)
        {
            LOG_ERROR("Register not written. addr + addr_offset < 0; addr: 0x%08x, offset: 0x%08x", addr, addrOffset);
            continue;
        }

        addr = addr + addrOffset;

        auto it = m_register_map.find(addr);
        if (it != m_register_map.end())
        {
            pRegister = *it;
        }

        if (!pRegister && name.size())
        {
            pRegister = findRegByLongName(name);
        }

        if (!pRegister)
        {
            LOG_WARN("no register found; addr: 0x%08x, name: %s", addr, name.toStdString().c_str());
            continue;
        }

        // Add the write request to our list in any case.
        // The write response will return the new value and cause the register to become updated.
        out_regs.append(reg_obj);

        if (pRegister->isMemoryRegister())
        {
            // For indirect RAM data registers, just copy the original request to the output request array.
            // The reasoning behind this is as follows:
            // The on-disk file may contain partial writes to the indirect RAM.
            // Note that the corresponding address register does not always count continuously.
            // It may have several slices with gaps in between (such as the WDEM QPC/Stg1C/Stg1W coefficient
            // RAM index registers).
            // In that situation it is virtually impossible to actually merge different write requests into our
            // internal data table before writing things out, because we cannot always reconstruct the mapping
            // between address register value and RAM cell index.
            //
            // So we let the REST server do the merging and read the RAMs back once all writes have been performed.
            if (!mems_to_read.contains(pRegister.get()))
            {
                mems_to_read.append(pRegister.get());
            }
        }
    }
    // Post the write request now.
    postWriteRegisters(out_regs);

    // Check whether there are any memories to read back.
    if (mems_to_read.size())
    {
        QJsonArray in_regs;
        for (Register * reg : mems_to_read)
        {
            QJsonObject read_req;
            reg->toJsonObjRead(read_req);
            in_regs.append(read_req);
        }
        postReadRegisters(in_regs);
    }
}

//
void RppRegistersModel::readl(RppRegistersModel::SharedRegPtr pRegister)
{
    LOG_INFO("", 0);

    m_read_regs_list.append(pRegister);

    handleReadRegisters();
}

//
void RppRegistersModel::handleReadRegisters()
{
    // Do nothing if the request-register list is empty.
    if (!m_read_regs_list.size())
    {
        return;
    }

    QJsonArray out_regs;

    while (m_read_regs_list.size())
    {
        auto         pRegister = m_read_regs_list.front();
        QJsonObject  o_reg;

        // If the max. number of registers has been reached or the following register is an 
        // indirect RAM data register, submit a request now with all there is.
        if (   (out_regs.count() >= MAX_NUM_REGISTERS_FOR_REST_CALL)
            || (out_regs.count() && pRegister->isMemoryRegister()  ) )
        {
            postReadRegisters(out_regs);
            out_regs = QJsonArray();
            continue;
        }
        
        m_read_regs_list.pop_front();

        // Otherwise read the register now.
        pRegister->toJsonObjRead(o_reg);
        out_regs.append(o_reg);


        // Send indirect memory reads alone.
        if (pRegister->isMemoryRegister())
        {
            postReadRegisters(out_regs);
            out_regs = QJsonArray();
        }
    }

    // Final flush, just in case there's still something in the register list.
    postReadRegisters(out_regs);
}

void RppRegistersModel::postReadRegisters(const QJsonArray & regs)
{
    // Check whether there's anything to do.
    if (regs.empty())
    {
        return;
    }

    QJsonObject o_readl;
    QJsonObject o_data;
    o_readl.insert("readl", regs);
    o_data.insert("data", o_readl);
    QJsonDocument doc(o_data);
    addRequest(m_cmd_readl, doc.toJson());
}

//
void RppRegistersModel::handleWriteRegisters()
{
    QJsonArray  out_regs;

    // Collect all registers to be written and generate proper writel() requests.
    while (!m_write_regs_list.empty())
    {
        auto         pRegister = m_write_regs_list.front();
        QJsonObject  o_reg;

        // If the maximum number of registers per REST request has been reached or the current register 
        // is an indirect memory write, post a request with the already collected registers.
        if (   (out_regs.count() >= MAX_NUM_REGISTERS_FOR_REST_CALL)
            || (out_regs.count() && pRegister->isMemoryRegister()  ) )
        {
            postWriteRegisters(out_regs);
            out_regs = QJsonArray();
            continue;
        }

        // The register is accepted now, clear it from the list of remaining register writes.
        m_write_regs_list.pop_front();

        // Special case: if it's a special memory address register, don't write that now, it's going 
        // to be handled by the data-register writes automatically.
        if (pRegister->isAddressRegister())
        {
            continue;
        }

        printf("write: 0x%08x=%s\n", pRegister->address(), pRegister->getValueAsString().toUtf8().constData());
        pRegister->toJsonObjWrite(o_reg);
        out_regs.append(o_reg);

        // Indirect memory registers shall be sent alone
        if (pRegister->isMemoryRegister())
        {
            postWriteRegisters(out_regs);
            out_regs = QJsonArray();
        }
    }

    // Flush any not-yet-committed register writes.
    postWriteRegisters(out_regs);
}

void RppRegistersModel::postWriteRegisters(const QJsonArray & regs)
{
    // Check whether there's anything to do.
    if (regs.empty())
    {
        return;
    }

    QJsonObject o_writel;
    QJsonObject o_data;
    o_writel.insert("writel", regs);
    o_data.insert("data", o_writel);
    QJsonDocument doc(o_data);
    addRequest(m_cmd_writel, doc.toJson());
}

void RppRegistersModel::addRequest(const QString & cmd, const QString & json)
{
    // Add the request to the request list.
    RestRequest req;
    req.cmd  = cmd;
    req.json = json;
    m_req_list.push_back(req);

    // Submit the request, provided no other request is currently active.
    doRequest();
}

void RppRegistersModel::doRequest()
{
    // If another request is in flight, return immediately.
    if (m_req_active)
    {
        return;
    }

    // If there's nothing to do, return now.
    if (m_req_list.empty())
    {
        setActiveStatus(false);
        setStatusColor("lightsteelblue");
        return;
    }

    RestRequest req = m_req_list.takeFirst();
    if (req.json.isNull()) {
        // This is a request with an empty body, likely the request to save the 
        // registers after all reads have been performed.
        if (m_save_regs_file.isOpen())
        {
            QJsonArray   jsonArray;

            for (auto pRegister: m_register_map)
            {
                // If the 'all' flag is not given, ignore any register that is read-only 
                // (cannot be programmed) or write-only (cannot read the value to program).
                if (pRegister && (m_save_regs_all || pRegister->isReadWrite()) )
                {
                    // Deliberately ignore indirect RAM address registers here, the 
                    // information  is contained in the corresponding data register.
                    if (pRegister->isAddressRegister())
                    {
                        continue;
                    }

                    QJsonObject o_reg;
                    pRegister->toJsonObjWrite(o_reg);
                    jsonArray.append(o_reg);
                }
            }

            QByteArray data = QJsonDocument(jsonArray).toJson();
            m_save_regs_file.write(data);
            m_save_regs_file.close();
        }
        // Recursively call ourselfs to handle the next request.
        doRequest();
        return;
    }

    LOG_INFO("%s", req.json.toStdString().c_str());

    Rest* r = new Rest(this);
    r->Post(m_board_address.c_str(), m_port, req.cmd.toStdString(), req.json.toUtf8());
    m_req_active = true;

    setActiveStatus(true);
    setStatusColor("yellow");
}


RppRegistersModel::SharedRegPtr RppRegistersModel::findRegByLongName(const QString& longName)
{
    for (auto pReg: m_register_map)
    {
        if (pReg && (pReg->longName() == longName))
        {
            return pReg;
        }
    }

    return SharedRegPtr();
}

//
void RppRegistersModel::reloadHistogram(HistogramIDs id)
{
    LOG_INFO("", 0);

    m_read_regs_list.append(m_HistogramRegList[id]);

    handleReadRegisters();
}

//
void RppRegistersModel::updateHistogramData(HistogramIDs id)
{
    LOG_INFO("", 0);

    m_HistogramData[id].clear();
    m_HistogramData[id].reserve(m_HistogramRegList[id].size());

    for (auto pReg: m_HistogramRegList[id])
    {
        uint32_t  value = 0;

        if (pReg->value().empty())
        {
            LOG_WARN("pReg->value().empty()", 0);
        }
        else
        {
            auto ok = true;
            value = pReg->value()[0].toUInt(&ok, 16);
            if (!ok)
            {
                LOG_WARN("pReg->value()[0].toUInt(&ok) failed; %s", pReg->value()[0].toStdString().c_str());
            }
        }
        m_HistogramData[id].push_back(value);
    }

    emit histogramDataUpdated(id, m_HistogramData[id]);
}

// This function updates the slices table when the register selection has changed
void RppRegistersModel::onClickedRegister(const int& row)
{
    LOG_INFO("row: %d", row);

    if (row == m_curr_register_index)
    {
        return;
    }

    m_curr_register_index = row;

    auto index      = m_register_table->index(row, 0);
    auto name       = m_register_table->data(index, ROLE_REG_NAME   ).toString();
    auto curr_value = m_register_table->data(index, ROLE_REG_VALUE  ).toString();
    auto address    = m_register_table->data(index, ROLE_REG_ADDRESS).toString();
    auto inst_addr  = address.toUInt(0, 16);

    LOG_INFO("selected  %s %s %s", name.toStdString().c_str(), address.toStdString().c_str(), curr_value.toStdString().c_str());

    m_slice_table->clean();

    auto it = m_register_map.find(inst_addr);

    if (it != m_register_map.end())
    {
        QList<Slice>      list;
        auto  pRegister = *it;

        m_field_description.clear();
        pRegister->getSliceList(list);
        for (const auto& slice: list)
        {
            QStringList stringList;
            stringList << slice.m_Name << slice.m_Mask << slice.m_Shift << "undefined";
            m_slice_table->append(stringList, slice.isWriteable());
            m_field_description << slice.m_Descr;
        }

        setDescription("");

        // If the register was not manually modified, re-read it. 
        // It might be a  volatile read-only register containing computed data.
        if (!pRegister->changed())
        {
            readl(pRegister);
        }
        else
        {
            if (pRegister->isMemoryRegister())
            {
                m_slice_table->setMemRegData(curr_value);
            }
            else
            {
                m_slice_table->setSliceData(curr_value);
            }
        }
    }
    else
    {
        LOG_ERROR("m_register_map.find(inst_addr) did not succees", 0);
    }
}

//
void RppRegistersModel::setDescription(QString description)
{
    LOG_INFO("", 0);

    if (m_curr_description != description)
    {
        m_curr_description = description;
        emit descriptionChanged();
    }
}

//
void RppRegistersModel::onClickedSlice(const int& index)
{
    LOG_INFO("", 0);

    if (m_field_description.empty() || index >= m_field_description.count())
    {
        setDescription("");
    }
    else
    {
        setDescription(m_field_description[index]);
    }
}

//
void RppRegistersModel::onRegisterValueChanged(const int& index, const QVariant &value)
{
    auto reg_index = m_register_table->index(index, 0);
    auto address   = m_register_table->data(reg_index, ROLE_REG_ADDRESS).toString().toUInt(0, 16);
    auto pRegister = m_register_map[address];

    if (pRegister)
    {
        printf("RegisterValueChanged...\n");
        bool wasChanged = pRegister->changed();
        if (pRegister->isMemoryRegister())
        {
            // TODO: pRegister->setValue();
            m_slice_table->setMemRegData(value);
        }
        else
        {
            pRegister->setValue(value.toString());
            m_slice_table->setSliceData(value);
        }

        if (!wasChanged && pRegister->changed())
        {
            m_register_table->setData(reg_index, "*", ROLE_REG_CHANGED);
        }
    }
    else
    {
        LOG_ERROR("m_register_map[address] returned NULL", 0);
    }
}


bool RppRegistersModel::isRegisterReadOnly(const int& index) const
{
    if (index < 0)
    {
        return true;
    }

    auto reg_index = m_register_table->index(index, 0);
    auto address   = m_register_table->data(reg_index, ROLE_REG_ADDRESS).toString().toUInt(0, 16);
    auto pRegister = m_register_map[address];

    return (pRegister) ? pRegister->isReadOnly() : true;
}

bool RppRegistersModel::isSliceReadOnly(const int &index) const
{
    if ((m_curr_register_index < 0) || (index < 0))
    {
        return true;
    }

    auto reg_index = m_register_table->index(m_curr_register_index, 0);
    auto address   = m_register_table->data(reg_index, ROLE_REG_ADDRESS).toString().toUInt(0, 16);
    auto pRegister = m_register_map[address];

    if (!pRegister)
    {
        return true;
    }

    QList<Slice> slist;
    pRegister->getSliceList(slist);
    return (index < slist.size()) ? slist[index].isReadOnly() : true;
}


//
void RppRegistersModel::onSliceValueChanged(const int& index, const QVariant &value)
{
    if (m_curr_register_index == -1)
    {
        LOG_ERROR("m_curr_register_index == -1", 0);
        return;
    }

    auto reg_index = m_register_table->index(m_curr_register_index, 0);
    auto address   = m_register_table->data(reg_index, ROLE_REG_ADDRESS).toString().toUInt(0, 16);
    auto pRegister = m_register_map[address];

    if (pRegister)
    {
        bool wasChanged = pRegister->changed();
        if (pRegister->isMemoryRegister())
        {
            pRegister->setValue(value.toString(), (uint32_t) index);
        }
        else
        {
            uint32_t reg_val = 0;

            for (const auto& slice: m_slice_table->entries())
            {
                uint32_t mask  = slice[1].toUInt(nullptr, 16);
                uint32_t shift = slice[2].toUInt(nullptr, 10);
                uint32_t value = slice[3].toUInt(nullptr, 16);

                reg_val |= (value << shift) & mask;
            }

            pRegister->setValue(QString("0x%1").arg(reg_val, 8, 16, QChar('0')));
        }

        m_register_table->setData(reg_index, pRegister->getValueAsString(), ROLE_REG_VALUE);
        if (!wasChanged && pRegister->changed())
        {
            m_register_table->setData(reg_index, "*", ROLE_REG_CHANGED);
        }
    }
    else
    {
        LOG_ERROR("m_register_map[address] returned NULL", 0);
    }
}

//
void RppRegistersModel::onInstanceIndexChanged(const int& index)
{
    LOG_INFO("", 0);

    if (m_register_json.empty())
    {
        return;
    }

    QJsonValue  value      = m_register_json[index];
    QJsonObject instance   = value.toObject();
    QString     offset_str = instance["offset"  ].toString();
    QString     name_str   = instance["name"    ].toString();
    QJsonArray  data_array = instance["register"].toArray();

    m_curr_instance_index = index;

    m_register_table->clean();
    m_slice_table   ->clean();

    if (data_array.isEmpty())
    {
        return;
    }

    uint32_t offset = offset_str.toInt(nullptr, 16);

    for (const QJsonValue& v : data_array)
    {
        QJsonObject reg      = v.toObject();
        QString     addr_str = reg["address"].toString();
        uint32_t    addr     = addr_str.toInt(nullptr, 16) + offset;
        QString     changed  = "";
        QString     value    = "undefined";

        addr_str = QString("0x%1").arg(addr, 8, 16, QChar('0'));

        auto pRegister = m_register_map[addr];
        if (pRegister)
        {
            if (!pRegister->changed())
            {
                m_read_regs_list.append(pRegister);
            }
            else
            {
                changed = "*";
                value = pRegister->getValueAsString();
            }
        }
        else
        {
            LOG_ERROR("m_register_map[addr] returned NULL",0);
        }

        QStringList val;
        val << reg["name"].toString(addr_str);
        val << addr_str;
        val << value;
        val << changed;
        m_register_table->append(val, pRegister->isWriteable());
    }

    m_curr_register_index = -1;

    handleReadRegisters();
}

//
void RppRegistersModel::onClickedRegisterRead()
{
    LOG_INFO("", 0);

    if (m_curr_register_index == -1)
    {
        LOG_ERROR("m_curr_register_index == -1", 0);
        return;
    }

    QModelIndex index     = m_register_table->index(m_curr_register_index, 0);
    QString     name      = m_register_table->data(index, ROLE_REG_NAME   ).toString();
    QString     address   = m_register_table->data(index, ROLE_REG_ADDRESS).toString();

    LOG_INFO("read %s %s", name.toStdString().c_str(), address.toStdString().c_str());

    auto pRegister = m_register_map[address.toUInt(nullptr, 0)];
    if (pRegister)
    {
        readl(pRegister);
    }
    else
    {
        LOG_ERROR("no register with address %s found", address.toStdString().c_str());
    }
}

//
void RppRegistersModel::onClickedRegisterWrite()
{
    LOG_INFO("", 0);

    // Loop over all current registers and check which are changed. If any is, put it 
    // on the list for writing, excluding those who are already scheduled for writing.
    for (auto pRegister : m_register_map)
    {
        if (!pRegister.get())
        {
            LOG_WARN("Empty entry in m_register_map", 0);
            continue;
        }

        if (pRegister->changed())
        {
            if (!m_write_regs_list.contains(pRegister))
            {
                m_write_regs_list.append(pRegister);
            }
        }
    }
    handleWriteRegisters();
}

//
void RppRegistersModel::setStatusColor(QColor color)
{
    if (m_status_color != color)
    {
        m_status_color = color;
        emit statusColorChanged();
    }
}

//
void RppRegistersModel::setActiveStatus(bool status)
{
    if (m_status_active != status)
    {
        m_status_active = status;
        emit statusActiveChanged();
    }
}

//
void RppRegistersModel::StopCurrentTasks()
{
    m_write_regs_list.clear();
    m_read_regs_list .clear();
    m_req_list       .clear();
    setActiveStatus(false);
}

//
QJsonDocument RppRegistersModel::Finished(QNetworkReply* reply)
{
    //LOG_INFO("", 0);

    QJsonDocument json;

    auto error = reply->error();

    if (error != QNetworkReply::NoError)
    {
        LOG_ERROR("reply->error(): %i - %s", error, reply->errorString().toStdString().c_str());
        //addMessageLogEntry("error", reply->errorString());
    }
    else
    {
        auto status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if (status != HTTP_OK)
        {
            LOG_ERROR("reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(): %i", status);
            //addMessageLogEntry("error", reply->errorString());
        }
        else
        {
            json = QJsonDocument::fromJson(reply->readAll());
        }
    }

    reply->deleteLater();

    return json;
}

//
void RppRegistersModel::restCallback(QNetworkReply* reply)
{
    //LOG_INFO("", 0);

    QJsonDocument jsdoc = Finished(reply);
    QJsonObject   jsobj = jsdoc.object();
    QJsonObject   data  = jsobj["data"].toObject();

    if (data.isEmpty())
    {
        LOG_WARN("reply is empty", 0);
        setStatusColor("red");
        StopCurrentTasks();
        return;
    }

    LOG_INFO("%s", jsdoc.toJson().toStdString().c_str());

    int         error        = data["error" ].toInt();
    QJsonArray  readl_array  = data["readl" ].toArray();
    QJsonArray  writel_array = data["writel"].toArray();

    if (error)
    {
        setStatusColor("red");
        QString info = data["info"].toString();
        LOG_ERROR("%s", info.toStdString().c_str());
        StopCurrentTasks();
        return;
    }

    auto do_update = [&](const QJsonArray & array, bool memValid) -> void {
        for (const QJsonValue & v : array) {
            auto reg_o      = v.toObject();
            auto addr_str   = reg_o["address"].toString();
            auto addr       = addr_str.toUInt(0, 16);
            auto pReg       = m_register_map[addr];
            auto val_a      = reg_o["value"].toArray();

            // check whether the register is known. if not, skip this item.
            if (!pReg)
            {
                continue;
            }

            // For indirect memory type addresses, the writel() response is empty and cannot be used to initialize
            // the value. Therefore, if such a thing is encountered, use the JSON encoding function of the register
            // to provide the values actually written, and initialize the register from these.
            if (!memValid && pReg->isMemoryRegister())
            {
                pReg->toJsonObjWrite(reg_o);
                val_a = reg_o["value"].toArray();
            }

            // Now update the register itself.
            pReg->setValue(val_a);

            // Check whether the register is in the current view. If not, there's nothing else to do.
            int instanceIdx = pReg->instanceIdx();
            if (instanceIdx != m_curr_instance_index)
            {
                continue;
            }

            // Get the register index from the register. It is needed to actually update the table view.
            int registerIdx = pReg->childIdx();
            auto viewIdx = m_register_table->index(registerIdx, 0);

            // If the index is not valid, the model may not yet have been filled.
            if (!viewIdx.isValid())
            {
                continue;
            }

            // Update the 'value' and 'modified' properties of the table view for the given register.
            m_register_table->setData(viewIdx, pReg->getValueAsString(), ROLE_REG_VALUE);
            m_register_table->setData(viewIdx, "", ROLE_REG_CHANGED);

            // If the register is currently selected, also update the slices.
            if (m_curr_register_index == registerIdx)
            {
                if (pReg->isMemoryRegister())
                {
                    m_slice_table->setMemRegData(pReg->getValueAsString());
                }
                else
                {
                    m_slice_table->setSliceData(pReg->getValueAsString());                    // TODO: still not correct
                }
            }
        }
    };

    if (!readl_array.isEmpty())
    {
        LOG_INFO("readl", 0);

        // In case of readl() commands, the indirect memory data will be intact.
        do_update(readl_array, true);
    }

    if (!writel_array.isEmpty())
    {
        LOG_INFO("writel", 0);

        // In case of writel() commands, the indirect memory data will be missing.
        do_update(writel_array, false);
    }

    // Clear the 'request is active' flag and submit the next queued request.
    m_req_active = false;
    doRequest();
}

