#ifndef RPP_REGISTERS_MODEL_H
#define RPP_REGISTERS_MODEL_H

#include <memory>
#include <QObject>
#include <QTime>
#include <QTimer>
#include <QColor>
#include <QFile>

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QSslConfiguration>
#include <QUrl>
#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonArray>

#include "ClassIDs.h"
#include "ListModel.h"
#include "TableModel.h"
#include "Rest.h"


namespace DCT { class Logger; }
class Register;
class Instance;


//
class RppRegistersModel : public RestCallback
{
    Q_OBJECT

    Q_PROPERTY(bool        status_active   READ getStatusActive   NOTIFY statusActiveChanged)
    Q_PROPERTY(QColor      status_color    READ getStatusColor    NOTIFY statusColorChanged)
    Q_PROPERTY(ListModel*  instance_list   READ getInstanceList   CONSTANT)
    Q_PROPERTY(TableModel* register_table  READ getRegisterTable  CONSTANT)
    Q_PROPERTY(TableModel* slice_table     READ getSliceTable     CONSTANT)
    Q_PROPERTY(QString     description     READ getDescription    NOTIFY descriptionChanged)

  public:
    typedef enum
    {
        HISTOGRAM_ID_PRE1,
        HISTOGRAM_ID_PRE2,
        HISTOGRAM_ID_PRE3,
        HISTOGRAM_ID_POST,

        NUM_HISTOGRAM_IDS
    } HistogramIDs;

    static const HistogramIDs  ALL_HISTOGRAM_IDS[4];

  public:
    RppRegistersModel(QObject* parent = nullptr);
    ~RppRegistersModel();
    void reset();

    void fillRegisterModels   (QString& val);
    void loadRegistersFromDisk(QString urlString, int addr_offset);
    void saveRegistersToDisk  (QString urlString, bool all);

    void reloadHistogram(HistogramIDs id);

    Q_INVOKABLE void onInstanceIndexChanged(const int& index);
    Q_INVOKABLE void onClickedRegister     (const int& index);
    Q_INVOKABLE void onClickedSlice        (const int& index);
    Q_INVOKABLE void onClickedRegisterWrite();
    Q_INVOKABLE void onClickedRegisterRead ();
    Q_INVOKABLE void onRegisterValueChanged(const int& index, const QVariant &value);
    Q_INVOKABLE void onSliceValueChanged   (const int& index, const QVariant &value);
    Q_INVOKABLE bool isRegisterReadOnly    (const int& index) const;
    Q_INVOKABLE bool isSliceReadOnly       (const int& index) const;

  public slots:
    bool        getStatusActive () const  { return m_status_active; }
    QColor      getStatusColor  () const  { return m_status_color; }
    ListModel*  getInstanceList () const  { return m_instance_list; }
    TableModel* getRegisterTable() const  { return m_register_table; }
    TableModel* getSliceTable   () const  { return m_slice_table; }
    QString     getDescription  () const  { return m_curr_description; }

  signals:
    void descriptionChanged();
    void statusActiveChanged();
    void statusColorChanged();
    void histogramDataUpdated(uint32_t id, const QVector<uint32_t>& data);

  private slots:
    void restCallback(QNetworkReply* reply);

  private:
    typedef std::shared_ptr<Register>           SharedRegPtr;
    typedef std::shared_ptr<Instance>           SharedInstPtr;
    typedef QMap<unsigned int, SharedInstPtr>   InstanceMap;            // key is instance base address
    typedef InstanceMap::iterator               InstanceMapIterator;
    typedef QMap<unsigned int, SharedRegPtr>    RegisterMap;            // key is absolute register address
    typedef RegisterMap::iterator               RegisterMapIterator;
    typedef QList<SharedRegPtr>                 RegisterList;
    typedef RegisterList::iterator              RegisterListIterator;
    typedef QVector<uint32_t>                   HistogramData;

    struct RestRequest {
        QString     cmd;
        QString     json;
    };

    void readl                 (SharedRegPtr pRegister);
    QJsonDocument  Finished    (QNetworkReply* reply);
    void setDescription        (QString description);
    void setStatusColor        (QColor color);
    void setActiveStatus       (bool status);
    void handleReadRegisters   ();
    void handleWriteRegisters  ();
    void postWriteRegisters    (const QJsonArray & regs);
    void postReadRegisters     (const QJsonArray & regs);
    void addRequest            (const QString & cmd, const QString & json);
    void doRequest             ();
    void StopCurrentTasks      ();
    void updateHistogramData   (HistogramIDs id);

    SharedRegPtr findRegByLongName(const QString& longName);

    typedef enum
    {
        ROLE_REG_NAME    = TableModel::Role0,
        ROLE_REG_ADDRESS = TableModel::Role1,
        ROLE_REG_VALUE   = TableModel::Role2,
        ROLE_REG_CHANGED = TableModel::Role3,

        ROLE_SLICE_NAME  = TableModel::Role0,
        ROLE_SLICE_MASK  = TableModel::Role1,
        ROLE_SLICE_SHIFT = TableModel::Role2,
        ROLE_SLICE_VALUE = TableModel::Role3,
    } RoleNames;

    enum
    {
        MAX_NUM_REGISTERS_FOR_REST_CALL =32,
    };

    const uint32_t      m_CLASS_ID = CLASS_ID_RPP_REGISTER_MODEL;
    DCT::Logger*        m_pLogger;

    QJsonArray          m_register_json;
    ListModel*          m_instance_list;
    InstanceMap         m_instance_map;
    RegisterMap         m_register_map;
    TableModel*         m_register_table;
    TableModel*         m_slice_table;
    int                 m_curr_instance_index;
    int                 m_curr_register_index;
    QStringList         m_field_description;
    QString             m_curr_description;
    std::string         m_board_address;
    int                 m_port;
    const char*         m_cmd_readl     = "/v1/rpp/readl";
    const char*         m_cmd_writel    = "/v1/rpp/writel";
    bool                m_status_active = false;
    QColor              m_status_color  = "red";
    QFile               m_save_regs_file;
    bool                m_save_regs_all;
    RegisterList        m_read_regs_list;
    RegisterList        m_write_regs_list;
    bool                m_req_active;
    QList<RestRequest>  m_req_list;
    HistogramData       m_HistogramData   [NUM_HISTOGRAM_IDS];
    RegisterList        m_HistogramRegList[NUM_HISTOGRAM_IDS];
};

#endif // RPP_REGISTERS_MODEL_H
