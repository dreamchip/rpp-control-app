import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQml.Models 2.2
import QtQuick.Extras 1.4

Item {
    id: element
    property alias registerTable: registerTable
    property alias sliceTable: sliceTable
    property alias instanceTable: instanceTable
    property alias textDescription: textDescription
    property alias btnWrite: btnWrite
    property alias btnRead: btnRead
    property alias statusIndicator: statusIndicator

    ColumnLayout {
        id: columnLayout
        width: parent.width
        height: parent.height - 5

        RowLayout {
            ComboBox {
                id: instanceTable
                Layout.fillWidth: true
                textRole: "type"
                model: regModel.instance_list
                onCurrentIndexChanged: {
                    regModel.onInstanceIndexChanged(instanceTable.currentIndex);
                }
            }
        }

        RowLayout {
            TableView {
                id: registerTable
                Layout.minimumWidth: 200
                Layout.preferredWidth: 400
                Layout.maximumWidth: 500
                Layout.fillWidth: true
                Layout.fillHeight: true
                model: regModel.register_table
                TableViewColumn {
                    role: "role0"
                    title: qsTr("Register")
                    width: 200
                }
                TableViewColumn {
                    role: "role1"
                    title: qsTr("Address")
                    width: 100
                }
                TableViewColumn {
                    role: "role2"
                    title: qsTr("Value")
                    width: 100
                    delegate: TextField {
                        text: styleData.value
                        placeholderText: "value"
                        enabled: !regModel.isRegisterReadOnly(styleData.row);
                        onActiveFocusChanged: {
                            if (activeFocus)
                            {
                                console.log("text field (1)")
                                regModel.onClickedRegister(styleData.row);
                            }
                        }
                        onTextChanged: {
                            if (activeFocus)
                            {
                                console.log("text field (2)")
                                regModel.onRegisterValueChanged(styleData.row, text);
                                regModel.register_table.setData(styleData.row, text, "role2");
                            }
                        }
                    }
                }
                TableViewColumn {
                    role: "role3"
                    title: qsTr("Modified")
                    width: 50
                }
                onClicked: {
                    console.log("table view 1")
                    regModel.onClickedRegister(row);
                }
                onActiveFocusChanged: {
                    if (activeFocus)
                    {
                        console.log("table view 2")
                        regModel.onClickedRegister(row);
                    }
                }
                onFocusChanged: {
                    if (focus)
                    {
                        console.log("table view 3")
                        regModel.onClickedRegister(row);
                    }
                }
            }
            TableView {
                id: sliceTable
                Layout.minimumWidth: 200
                Layout.preferredWidth: 370
                Layout.maximumWidth: 500
                Layout.fillHeight: true
                Layout.fillWidth: true
                model: regModel.slice_table
                TableViewColumn {
                    role: "role0"
                    title: qsTr("Slice")
                    width: 150
                }
                TableViewColumn {
                    role: "role1"
                    title: qsTr("Mask")
                    width: 80
                }
                TableViewColumn {
                    role: "role2"
                    title: qsTr("Shift")
                    width: 40
                }
                TableViewColumn {
                    role: "role3"
                    title: qsTr("Value")
                    width: 100
                    delegate: TextField {
                        text: styleData.value
                        placeholderText: "value"
                        enabled: !regModel.isSliceReadOnly(styleData.row);
                        onActiveFocusChanged: {
                            if (activeFocus)
                            {
                                console.log("text field (3)")
                                regModel.onClickedSlice(styleData.row);
                            }
                        }
                        onTextChanged: {
                            if (activeFocus)
                            {
                                console.log("text field (5)")
                                regModel.slice_table.setData(styleData.row, text, "role3");
                                regModel.onSliceValueChanged(styleData.row, text);
                            }
                        }
                    }
                }
                onClicked: {
                    regModel.onClickedSlice(row);
                }
            }
        }

        RowLayout {
            TextArea {
                id: textDescription
                Layout.fillWidth: true
                text: regModel.description
            }
        }
        RowLayout {

            Button {
                id: btnWrite
                text: qsTr("Write")
                onClicked: {
                    regModel.onClickedRegisterWrite();
                }
            }

            Button {
                id: btnRead
                text: qsTr("Read")
                onClicked: {
                    regModel.onClickedRegisterRead();
                }
            }

            StatusIndicator {
                id: statusIndicator
                color: regModel.status_color
                active: regModel.status_active
                Layout.preferredHeight: 20
                Layout.preferredWidth: 20
            }
        }
    }
}
