#include "Schema.h"

//
const std::string schema_readl = R"X(
 {
    "title"      : "the root schema",
    "description": "readl schema",

    "examples": [
        {
            "data":  {
                "readl" : [
                    {
                        "address": "0x10050",
                        "length": "16"
                    }
                ]
            }
        }
    ],

    "type": "object",
    "properties": {
        "data": {
            "type": "object",
            "properties": {
                "readl": {
                    "type": "array",
                    "items": {
                        "anyOf": [
                            {
                                "type": "object",
                                "properties": {
                                    "address": { "type": "string" },
                                    "length" : { "type": "string" }
                                }
                                "required": [ "address", "length" ],
                                "additionalProperties": true,
                            }
                        ]
                    }
                    "additionalItems": true,
                }
            }
            "required": [ "readl" ],
            "additionalProperties": false,
        }
    }
    "additionalProperties": false,
    "required": [ "data" ],
 }
)X";

//
const std::string schema_writel = R"X(
 {
    "title"      : "the root schema",
    "description": "writel schema",
    "examples": [
        {
            "data": {
                "writel": [
                    {
                        "address": "0x10050",
                        "length": "4",
                        "value": [
                            {
                                "0": "0x33"
                            }
                        ]
                    }
                ]
            }
        }
    ],
    "type": "object",
    "additionalProperties": false,
    "required": [ "data" ],
    "properties": {
        "data": {
            "type": "object",
            "required": [ "writel" ],
            "additionalProperties": false,
            "properties": {
                "writel": {
                    "type": "array",
                    "additionalItems": true,
                    "items": {
                        "anyOf": [
                            {
                                "type": "object",
                                "required": [ "address", "length"],
                                "additionalProperties": true,
                                "properties": {
                                    "address": { "type": "string" },
                                    "length" : { "type": "string" },
                                    "value"  : {
                                        "type": "array",
                                        "items": { "value": "string" }
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
    }
 }
)X";

//
const std::string schema_func = R"X(
 {
    "title"      : "the root schema",
    "description": "function schema",

    "examples": [
        {
            "data": {
                "func": [
                    {
                        "name": "aec_configure",
                        "parameter": [
                            { "name": "n_samples", "type": "u32", "value": "921600" },
                            { "name": "mu_day"   , "type": "u32", "value":     "16" },
                            { "name": "mu_night" , "type": "u32", "value":      "8" },
                            { "name": "sigma"    , "type": "u32", "value":      "2" },
                            { "name": "n_data"   , "type": "u32", "value":      "3" },
                            { "name": "data"     , "type": "u32", "values": ["2", "3", "4"] }
                        ]
                    }
                ]
            }
        }
    ],

    "type": "object",
    "properties": {
        "data": {
            "type": "object",
            "properties": {
                "func": {
                    "type": "array",
                    "items": {
                        "anyOf": [
                            {
                                "type": "object",
                                "properties": {
                                    "name": { "type": "string" },
                                    "parameter": {
                                        "type": "array",
                                        "items": {
                                            "anyOf": [
                                                {
                                                    "type": "object",
                                                    "properties": {
                                                        "name": { "type": "string" },
                                                        "type": { "type": "string" },
                                                        "oneof" : [
                                                            "value": { "type" : "string" },
                                                            "values": {
                                                                "type": "array",
                                                                "items": {
                                                                    "type": "string"
                                                                }
                                                                "additionalItems": false,
                                                            }
                                                        ]
                                                    }
                                                    "default": { },
                                                    "required": [ "name", "type" ],
                                                    "additionalProperties": true,
                                                }
                                            ]
                                        }
                                        "default": [],
                                        "additionalItems": true,
                                    }
                                }
                                "required": [ "name", "parameter"],
                                "additionalProperties": true,
                            }
                        ]
                    }
                    "additionalItems": true,
                }
            }
            "required": [ "func" ],
            "additionalProperties": false,
        }
    }
    "additionalProperties": false,
    "required": [ "data" ],
 }
)X";

//
const std::string schema_saved_register_values = R"X(
 {
    "title"      : "the root schema",
    "description": "saved register values schema",
    "examples": [
        {
            "name"   : "avdif_read_ctrl_reg",       # optional
            "address": "0x00010044",
            "length" : "4",
            "value"  : [ "0x00" ]
        }
    ],
 }
)X";

