#ifndef SCHEMA_H
#define SCHEMA_H
#
#include <string>

extern const std::string schema_readl;
extern const std::string schema_writel;
extern const std::string schema_func;

#endif // SCHEMA_H
