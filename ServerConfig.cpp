#include <QDebug>
#include <QJsonObject>
#include <QVariant>
#include <QFile>
#include <QSettings>
#include <QCoreApplication>
#include <sstream>

#include "ServerConfig.h"
#include "Logger.h"
#include "ClassIDs.h"
#include "Rest.h"
#include "Schema.h"
#include "Function.h"
#include "RppFunctionsModel.h"
#include "RppRegistersModel.h"
#include "Settings.h"

#if !WIN32
#include <unistd.h>
#endif

//
ServerConfig::ServerConfig(RppFunctionsModel* funcModel, RppRegistersModel* regModel, QObject* parent)
  : RestCallback(parent)
  , m_pLogger   (new DCT::Logger("App"))
  , m_pFuncModel(funcModel)
  , m_pRegModel (regModel)
{
    LOG_INFO("", 0);

    reset();
}

//
ServerConfig::~ServerConfig()
{
    LOG_INFO("", 0);

    delete m_pLogger;
}

//
void ServerConfig::reset()
{
    LOG_INFO("", 0);

    m_pFuncModel->reset();
    m_pRegModel ->reset();

    Settings settings;
    m_board_address = settings.targetIpAddress().toStdString();
    m_port          = settings.targetIpPort().toInt();

    requestConfigInfo();
}

//
void ServerConfig::requestConfigInfo()
{
    requestConfig("cfg_info");
}

//
void ServerConfig::requestFunctionConfig()
{
    requestConfig("cfg_function");
}

//
void ServerConfig::requestRegisterConfig()
{
    requestConfig("cfg_register");
}

void ServerConfig::requestConfig(const QString& name)
{
    QJsonArray  para_array;
    QJsonObject para_data;
    para_data.insert("name", "data");
    para_data.insert("mode", "r");
    para_data.insert("type", "string");
    para_array.append(para_data);

    callFunction(name, para_array);
}

void ServerConfig::callFunction(const QString& name, const QJsonArray& parameters)
{
    //LOG_INFO("", 0);

    QJsonObject data;
    QJsonArray  func_array;
    QJsonObject func;
    QJsonObject payload;

    func.insert("name", name);
    func.insert("parameter", parameters);
    func_array.append(func);
    payload.insert("func", func_array);
    data.insert("data", payload);
    QJsonDocument doc(data);

    //LOG_INFO("%s", doc.toJson().toStdString().c_str());

    Rest* r = new Rest(this);
    r->Post(m_board_address.c_str(), m_port, m_cmd_func, doc.toJson());
}

//
QJsonDocument ServerConfig::Finished(QNetworkReply* reply)
{
    //LOG_INFO("", 0);

    QJsonDocument json;

    auto error = reply->error();

    if (error != QNetworkReply::NoError)
    {
        LOG_ERROR("reply->error(): %i - %s", error, reply->errorString().toStdString().c_str());
    }
    else
    {
        auto status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if (status != HTTP_OK)
        {
            LOG_ERROR("reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(): %i", status);
        }
        else
        {
            json = QJsonDocument::fromJson(reply->readAll());
        }
    }

    reply->deleteLater();

    return json;
}

//
void ServerConfig::restCallback(QNetworkReply* reply)
{
    //LOG_INFO("", 0);

    QJsonDocument jsdoc = Finished(reply);
    QJsonObject   jsobj = jsdoc.object();
    QJsonObject   data  = jsobj["data"].toObject();

    if (data.isEmpty())
    {
        LOG_WARN("reply is empty", "");
        return;
    }

    //LOG_INFO("%s", jsdoc.toJson().toStdString().c_str());

    auto error      = data["error"].toInt();
    auto func_array = data["func" ].toArray();

    if (error)
    {
        auto info = data["info"].toString();
        LOG_ERROR("%s", info.toStdString().c_str());
    }

    if (!func_array.isEmpty())
    {
        LOG_INFO("func", 0);

        for (const QJsonValue& v : func_array)
        {
            auto func    = v.toObject();
            auto name    = func["name"].toString();
            auto param_a = func["parameter"].toArray();

            if (param_a.empty())
            {
                continue;
            }

            LOG_INFO("func %s", name.toStdString().c_str());

            if (name == "cfg_info")
            {
                auto value     = param_a[0].toObject()["value"].toString();
                LOG_INFO("%s", value.toStdString().c_str());
                auto bytearray = value.toUtf8();
                auto jsdoc     = QJsonDocument::fromJson(bytearray);
                auto jsarray   = jsdoc.array();
                if (!jsarray.empty())
                {
                    auto jsvalue  = jsarray[0];
                    auto jsobject = jsvalue.toObject();
                    m_ServerIdent.fromJson(jsobject);
                }

                requestFunctionConfig();
            }
            else if (name == "cfg_function")
            {
                auto value = param_a[0].toObject()["value"].toString();
                m_pFuncModel->fillFunctionModels(value);

                requestRegisterConfig();
            }
            else if (name == "cfg_register")
            {
                auto value = param_a[0].toObject()["value"].toString();
                m_pRegModel->fillRegisterModels(value);
            }
        }
    }
}

QString ServerConfig::getIdentString()
{
    return m_ServerIdent.toString();
}
