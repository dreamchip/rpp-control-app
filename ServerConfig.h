#ifndef SERVER_CONFIG_H
#define SERVER_CONFIG_H

#include <memory>
#include <QObject>
#include <QTime>
#include <QTimer>
#include <QColor>
#include <QFile>

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QSslConfiguration>
#include <QUrl>
#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonArray>

#include "ClassIDs.h"
#include "ServerIdent.h"
#include "Rest.h"


namespace DCT { class Logger; }
class Function;
class RppFunctionsModel;
class RppRegistersModel;

//
class ServerConfig : public RestCallback
{
    Q_OBJECT

  public:
    explicit ServerConfig(RppFunctionsModel* funcModel, RppRegistersModel* regModel, QObject* parent = nullptr);
    ~ServerConfig();
    void reset();

    QString getIdentString();

  public slots:
    void restCallback(QNetworkReply* reply);

  private:
    void requestConfigInfo    ();
    void requestFunctionConfig();
    void requestRegisterConfig();
    void requestConfig        (const QString& name);
    void callFunction         (const QString& name, const QJsonArray& parameters = QJsonArray());
    QJsonDocument Finished    (QNetworkReply* reply);

    const uint32_t      m_CLASS_ID = CLASS_ID_CONFIG_APP;
    DCT::Logger*        m_pLogger;
    std::string         m_board_address;
    int                 m_port;
    const char*         m_cmd_func = "/v1/rpp/func";
    ServerIdent         m_ServerIdent;
    RppFunctionsModel*  m_pFuncModel;
    RppRegistersModel*  m_pRegModel;
};

#endif // SERVER_CONFIG_H
