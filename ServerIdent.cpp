#include <QJsonDocument>
#include "QJsonObject"
#include "QJsonArray"
#include "ServerIdent.h"
#include "Logger.h"

//
ServerIdent::ServerIdent()
 : m_pLogger(new DCT::Logger(""))
{
    LOG_INFO("", 0);
}

//
ServerIdent::~ServerIdent()
{
    LOG_INFO("", 0);

    delete m_pLogger;
}

void ServerIdent::fromString(const QString& string)
{
    LOG_INFO("%s", string.toStdString().c_str());

    QJsonDocument jsdoc = QJsonDocument::fromJson(string.toUtf8());
    fromJson(jsdoc.array()[0].toObject());
}

void ServerIdent::fromJson(const QJsonObject &jsonObject)
{
    m_Name       = jsonObject["name"      ].toString();
    m_Version    = jsonObject["version"   ].toString();
    m_Repository = jsonObject["repository"].toString();
    m_Revision   = jsonObject["revision"  ].toString();
    m_Date       = jsonObject["date"      ].toString();

    LOG_INFO("%s, %s, %s, %s, %s",
             m_Name      .toStdString().c_str(),
             m_Version   .toStdString().c_str(),
             m_Repository.toStdString().c_str(),
             m_Revision  .toStdString().c_str(),
             m_Date      .toStdString().c_str() );
}

QString ServerIdent::toString()
{
    return  "<br>" + m_Name
          + "<br>" + m_Version
          + "<br>" + m_Repository
        //+ "<br>" + m_Revision
          + "<br>" + m_Date;
}
