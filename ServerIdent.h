#ifndef SERVER_IDENT_H
#define SERVER_IDENT_H

#include <cstdint>
#include <QString>
#include <QJsonObject>
#include "ClassIDs.h"


namespace DCT { class Logger; }


class ServerIdent
{
  public:
    ServerIdent();

    virtual ~ServerIdent();

    void fromString(const QString& string);

    void fromJson(const QJsonObject& jsonObject);

    QString toString();

    const QString& name      ()  const  { return m_Name      ; }
    const QString& version   ()  const  { return m_Version   ; }
    const QString& repository()  const  { return m_Repository; }
    const QString& revision  ()  const  { return m_Revision  ; }
    const QString& date      ()  const  { return m_Date      ; }

  protected:
    const uint32_t  m_CLASS_ID = CLASS_ID_SERVER_IDENT;
    DCT::Logger*    m_pLogger;
    QString         m_Name;
    QString         m_Version;
    QString         m_Repository;
    QString         m_Revision;
    QString         m_Date;
};

#endif // SERVER_IDENT_H
