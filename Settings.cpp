#include "Settings.h"

const QString Settings::GEOMETRY       = "geometry";
const QString Settings::IP_ADDRESS     = "target_ip_address";
const QString Settings::IP_PORT        = "target_ip_port";
const QString Settings::ADDRESS_OFFSET = "base_address_offset";

//
Settings::Settings()
{
}

//
QByteArray  Settings::geometry() const
{
    return m_Settings.value(GEOMETRY, QByteArray()).toByteArray();
}

//
void  Settings::setGeometry(const QByteArray& geometry)
{
    m_Settings.setValue(GEOMETRY, geometry);
}

//
QString  Settings::targetIpAddress() const
{
    return m_Settings.value(IP_ADDRESS, "127.0.0.1").toString();
}

//
void  Settings::setTargetIpAddress(const QString& addr)
{
    m_Settings.setValue(IP_ADDRESS, addr);
}

//
QString  Settings::targetIpPort() const
{
    return m_Settings.value(IP_PORT, "2301").toString();
}

//
void  Settings::setTargetIpPort(const QString& port)
{
    m_Settings.setValue(IP_PORT, port);
}

//
QString  Settings::addressOffset() const
{
    return m_Settings.value(ADDRESS_OFFSET, "0x0000").toString();
}

//
void  Settings::setAddressOffset(const QString& offset)
{
    m_Settings.setValue(ADDRESS_OFFSET, offset);
}
