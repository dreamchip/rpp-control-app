#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include <QByteArray>
#include <QSettings>


class Settings
{
  public:
    Settings();

    QByteArray  geometry       () const;
    QString     targetIpAddress() const;
    QString     targetIpPort   () const;
    QString     addressOffset  () const;

    void  setGeometry       (const QByteArray& geometry);
    void  setTargetIpAddress(const QString&    addr);
    void  setTargetIpPort   (const QString&    port);
    void  setAddressOffset  (const QString&    offset);

  protected:
    QSettings  m_Settings;

    static const QString GEOMETRY;
    static const QString IP_ADDRESS;
    static const QString IP_PORT;
    static const QString ADDRESS_OFFSET;
};

#endif // SETTINGS_H
