#include <QLineEdit>
#include <QSettings>
#include <QIntValidator>
#include "stdint.h"
#include <QHBoxLayout>
#include <QFont>
#include <QLabel>
#include <QKeyEvent>
#include <QPushButton>
#include <QDialogButtonBox>

#include "SettingsDialog.h"
#include "Settings.h"

//
SettingsDialog::SettingsDialog(QWidget *parent)
  : QDialog(parent)
{
    Settings settings;
    auto board_address = settings.targetIpAddress();
    auto port          = settings.targetIpPort();
    auto substrings    = board_address.split('.');
    auto pMainLayout   = new QVBoxLayout();
    auto pIpLayout     = new QHBoxLayout();
    auto pLabel        = new QLabel("Server IP address: ");
    auto pDot          = new QLabel(".");
    auto pColon        = new QLabel(":");
    auto buttonBox     = new QDialogButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Cancel);

    substrings.append(port);

    setLayout(pMainLayout);

    pIpLayout->addWidget(pLabel);

    // 4 edit controls for ip address
    for (int i = 0; i < NUM_LINE_EDITS; ++i)
    {
        if (i == LINE_EDIT_PORT)
        {
            pIpLayout->addWidget(pColon);
        }
        else if (i > 0)
        {
            pIpLayout->addWidget(pDot);
        }

        auto pLineEdit = m_pLineEdit[i] = new QLineEdit();

        if (i == LINE_EDIT_PORT)
        {
            QRegExp rx1("^(0|[1-9]|[1-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9][0-9][0-9]|[1-5][0-9][0-9][0-9][0-9]|6[0-4][0-9][0-9][0-9]|65[0-4][0-9][0-9]|655[0-2][0-9]|6553[0-5])$");
            auto validator = new QRegExpValidator(rx1, pLineEdit);
            pLineEdit->setValidator(validator);
            pLineEdit->setMaximumWidth(50);
        }
        else
        {
            QRegExp rx("^(0|[1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$");
            auto validator = new QRegExpValidator(rx, pLineEdit);
            pLineEdit->setValidator(validator);
            pLineEdit->setMaximumWidth(40);
        }

        auto font = pLineEdit->font();
        font.setStyleHint(QFont::Monospace);
        font.setFixedPitch(true);
        pLineEdit->setFont(font);
        pLineEdit->setAlignment(Qt::AlignCenter);
        pLineEdit->setText(substrings[i]);

        pIpLayout->addWidget(pLineEdit);
    }

    m_pLineEdit[LINE_EDIT_IP_0]->installEventFilter(this);

    pIpLayout->addStretch();

    pMainLayout->addLayout(pIpLayout);
    pMainLayout->addWidget(buttonBox);

    connect(buttonBox, &QDialogButtonBox::accepted, this, &SettingsDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &SettingsDialog::reject);
}

//
SettingsDialog::~SettingsDialog()
{
}

//
void SettingsDialog::accept()
{
    auto board_address = QString("%1.%2.%3.%4").arg(m_pLineEdit[0]->text(),
                                                    m_pLineEdit[1]->text(),
                                                    m_pLineEdit[2]->text(),
                                                    m_pLineEdit[3]->text());
    auto port          = m_pLineEdit[4]->text();

    Settings settings;
    settings.setTargetIpAddress(board_address);
    settings.setTargetIpPort(port);

    QDialog::accept();
}

//
bool SettingsDialog::eventFilter(QObject *obj, QEvent *event)
{
    bool bRes = QWidget::eventFilter(obj, event);

    if (   (event->type()  == QEvent::Show)
        && (m_pLineEdit[0] == obj         ) )
    {
        m_pLineEdit[0]->setFocus();
        m_pLineEdit[0]->setCursorPosition(0);
        m_pLineEdit[0]->selectAll();
    }

    return bRes;
}
