#ifndef SETTINGS_DIALOG_H
#define SETTINGS_DIALOG_H

#include <QDialog>

class QLineEdit;


class SettingsDialog : public QDialog
{
    Q_OBJECT

  public:
    explicit SettingsDialog(QWidget *parent = nullptr);
    virtual ~SettingsDialog();

    virtual bool eventFilter(QObject *obj, QEvent *event) override;

    void accept() override;

  private:
    enum
    {
        LINE_EDIT_IP_0  = 0,
        LINE_EDIT_IP_1,
        LINE_EDIT_IP_2,
        LINE_EDIT_IP_3,
        LINE_EDIT_PORT,
        NUM_LINE_EDITS,

        MAX_DIGITS_IP   = 3,
        MAX_DIGITS_PORT = 5,
    };

    QLineEdit* m_pLineEdit[NUM_LINE_EDITS];
};

#endif // SETTINGS_DIALOG_H
