#include <QDebug>
#include "TableModel.h"
#include "Logger.h"

TableModel::TableModel(unsigned columnCount, QObject *parent, const char *moduleName)
  : QAbstractTableModel(parent)
  , m_pLogger(new DCT::Logger(moduleName))
{
    LOG_INFO("", "");

    m_columnCount = columnCount;
}

TableModel::~TableModel()
{
    m_entries.clear();

    LOG_INFO("", "");
    delete m_pLogger;
}

void TableModel::append(const QStringList &val, bool writeable)
{
    //LOG_INFO("%s", val.join(" ").toStdString().c_str());

    beginInsertRows(QModelIndex(), m_entries.size(), m_entries.size());
    m_entries.append(val);
    // by default, do not disallow editing.
    m_writeable.append(writeable);
    endInsertRows();
}

void TableModel::remove(int row)
{
    //LOG_INFO("", "");

    if (row < 0 || row >= m_entries.size()) {
        return;
    }

    beginRemoveRows(QModelIndex(), row, row);
    m_entries.removeAt(row);
    m_writeable.removeAt(row);
    endRemoveRows();
}

void TableModel::remove_first()
{
    //LOG_INFO("", "");

    if (m_entries.isEmpty()) {
        return;
    }

    beginRemoveRows(QModelIndex(), 0, 0);
    m_entries.removeLast();
    m_writeable.removeLast();
    endRemoveRows();
}

void TableModel::clean()
{
    //LOG_INFO("", "");

    if (m_entries.isEmpty()) {
        return;
    }

    beginRemoveRows(QModelIndex(), 0, m_entries.count()-1);
    m_entries.clear();
    m_writeable.clear();
    endRemoveRows();
}

int TableModel::rowCount() const
{
    //LOG_INFO("", "");

    return m_entries.size();
}

int TableModel::rowCount(const QModelIndex &parent) const
{
    //LOG_INFO("", "");

    Q_UNUSED(parent);
    return m_entries.size();
}

int TableModel::columnCount() const
{
    //LOG_INFO("", "");

    return m_columnCount;
}

int TableModel::columnCount(const QModelIndex &parent) const
{
    //LOG_INFO("", "");

    Q_UNUSED(parent);
    return m_columnCount;
}

QVariant TableModel::data(const QModelIndex &index, int role) const
{
    //LOG_INFO("", "");

    if (index.row() < 0 || index.row() >= m_entries.size()) {
        return QVariant();
    }

    const auto &entry = m_entries[index.row()];
    unsigned col = role - Role0;
    if (col >= m_columnCount)
        return QVariant();

    return entry[col];
}

QHash<int, QByteArray> TableModel::roleNames() const
{
    //LOG_INFO("", "");

    QHash<int, QByteArray> roles;
    roles[Role0] = "role0";
    roles[Role1] = "role1";
    roles[Role2] = "role2";
    roles[Role3] = "role3";
    roles[Role4] = "role4";
    return roles;
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    //LOG_INFO("1", "");

    auto row = index.row();

    if (row < 0 || row >= m_entries.size())
    {
        LOG_ERROR("Invalid row %i in QModelIndex", row);
        return false;
    }

    auto& entry = m_entries[row];
    unsigned col = role - Role0;

    if (col >= m_columnCount)
    {
        LOG_ERROR("Invalid role %d", role);
        return false;
    }

    entry[col] = value.toString();

    emit dataChanged(index, index, { role });

    return true;
}

bool TableModel::setData(const int row, const QVariant &value, QString role)
{
    LOG_INFO("2; row: %d, role: %s, val: %s", row, role.toStdString().c_str(), value.toString().toStdString().c_str());

    if (row < 0 || row >= m_entries.size())
    {
        //LOG_WARN("Invalid row %i", row);
        return false;
    }

    auto& entry = m_entries[row];

    Roles role_id;

    if      (role == "role0")  { role_id = Role0; }
    else if (role == "role1")  { role_id = Role1; }
    else if (role == "role2")  { role_id = Role2; }
    else if (role == "role3")  { role_id = Role3; }
    else if (role == "role4")  { role_id = Role4; }
    else
    {
        LOG_ERROR("Invalid role string %s", role.toStdString().c_str());
        return false;
    }

    entry[role_id - Role0] = value.toString();

    auto index = QAbstractTableModel::index(row, 0, QModelIndex());

    emit dataChanged(index, index, { role_id });

    return true;
}

bool TableModel::setSliceData(const QVariant &value)
{
    //LOG_INFO("", "");

    if (m_entries.empty())
    {
        return false;
    }

    uint32_t val = value.toString().toUInt(nullptr, 16);

    for (auto& entry: m_entries)
    {
        uint32_t mask  = entry[1].toUInt(nullptr, 16);
        uint32_t shift = entry[2].toUInt();
        uint32_t v = (val & mask) >> shift;

        entry[3] = QString("0x%1").arg(v, 4, 16, QChar('0'));
    }

    emit dataChanged(index(0                 , 0, QModelIndex()),
                     index(m_entries.size()-1, 0, QModelIndex()),
                     {Role0, Role3});

    return true;
}

bool TableModel::setMemRegData(const QVariant &value)
{
    //LOG_INFO("", "");

    if (m_entries.empty())
    {
        return false;
    }

    const auto& value_list = value.toString().split(",");
    auto        num_values = value_list.size();

    for (int i = 0; i < m_entries.size(); i++)
    {
        m_entries[i][3] = (i < num_values) ? value_list[i] : "undefined";
    }

    emit dataChanged(index(0                 , 0, QModelIndex()),
                     index(m_entries.size()-1, 0, QModelIndex()),
                     {Role0, Role3});

    return true;
}

Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
    LOG_INFO("", "");
    printf("row=%d, writeable=%d.\n", index.row(), m_writeable[index.row()]);
    if (m_writeable[index.row()])
    {
        return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
    }
    return QAbstractTableModel::flags(index) & ~Qt::ItemIsEditable;
}
