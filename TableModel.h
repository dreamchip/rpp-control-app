#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include "ClassIDs.h"


namespace DCT { class Logger; }


class TableModel : public QAbstractTableModel
{
    Q_OBJECT

  public:
    enum Roles {
        Role0 = Qt::UserRole + 1,
        Role1,
        Role2,
        Role3,
        Role4
    };

    TableModel(unsigned columnCount, QObject *parent = nullptr, const char *moduleName = "");

    virtual ~TableModel();

    Q_INVOKABLE void append(const QStringList &val, bool writeable = true);
    Q_INVOKABLE void remove(int row);
    Q_INVOKABLE void remove_first();
    Q_INVOKABLE void clean();

    int rowCount() const;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount() const;
    int columnCount(const QModelIndex &parent) const override;

    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    bool setSliceData(const QVariant &value);
    bool setMemRegData(const QVariant &value);

    Q_INVOKABLE bool setData(const int row, const QVariant &value, QString role);

    Qt::ItemFlags flags(const QModelIndex &index) const override;

    unsigned size()  { return m_entries.size(); }

    const QList<QStringList>& entries()  { return m_entries; }

  protected:
    QHash<int, QByteArray> roleNames() const override;

  protected:
    const uint32_t  m_CLASS_ID = CLASS_ID_TABLEMODEL;
    DCT::Logger*    m_pLogger;
    unsigned        m_columnCount;

  private:
    QList<QStringList> m_entries;
    QList<bool>        m_writeable;

};

#endif // TABLEMODEL_H
