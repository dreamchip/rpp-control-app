/******************************************************************************
*
* Copyright 2018,2019, Dream Chip Technologies GmbH. All rights reserved.
* No part of this work may be reproduced, modified, distributed, transmitted,
* transcribed, or translated into any language or computer format, in any form
* or by any means without written permission of:
* Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
* Germany
*
*****************************************************************************/
/**
* @file   time_measurement.cpp
*
* @brief  
*
*****************************************************************************/
#if WIN32
# include <windows.h>
#else
# include <sys/time.h>
#endif

#include "TimeMeasurement.h"
#include "ClassIDs.h"


namespace DCT
{

uint64_t TimeMeasurement::m_ticks_per_sec  = 0;
bool     TimeMeasurement::m_initialized    = false;

// The standard constructor
TimeMeasurement::TimeMeasurement()
  : m_CLASS_ID(CLASS_ID_TIME_MEASUREMENT)
  , m_start_time_us(0)
{
    (void) m_CLASS_ID;

  #if !WIN32
    m_ticks_per_sec  = MICROSECONDS_PER_SECOND;
    m_initialized    = true;
  #endif

    startMeasurement();
}

// The time measurement is started
void TimeMeasurement::startMeasurement()
{
    m_start_time_us = getCurrentTimeUS();
}

// Returns the number of microseconds since the measurement was started
uint64_t TimeMeasurement::getMicrosecondsSinceStart()
{
    return getCurrentTimeUS() - m_start_time_us;
}

//
int64_t TimeMeasurement::setMicrosecondsSinceStart(uint64_t usec)
{
    auto curr     = (int64_t) getMicrosecondsSinceStart();
    auto corr     = usec - curr; // usec = curr + corr
    auto newStart = (int64_t)m_start_time_us - corr;

    if (newStart < 0)
    {
        m_start_time_us = 0;
    }
    else
    {
        m_start_time_us = (uint64_t)newStart;
    }

    return corr;
}

//
void TimeMeasurement::waitUntilUS(uint64_t usec)
{
    while (getMicrosecondsSinceStart() < usec);
}

void TimeMeasurement::waitUntilAbsUS(uint64_t usec)
{
    while (getCurrentTimeUS() < usec);
}

// Returns the current time in microsecconds
uint64_t TimeMeasurement::getCurrentTimeUS() // static
{
  #if WIN32
    if (!m_initialized)
    {
        LARGE_INTEGER  performance_counter_frequency; // Counts per second
        QueryPerformanceFrequency(&performance_counter_frequency);
        m_ticks_per_sec  = performance_counter_frequency.QuadPart;
        m_initialized    = true;
    }
    LARGE_INTEGER  counter;
    QueryPerformanceCounter(&counter);
    return counter.QuadPart * MICROSECONDS_PER_SECOND / m_ticks_per_sec;
  #else
    struct timeval time;
    gettimeofday(&time, nullptr);
    return MICROSECONDS_PER_SECOND * time.tv_sec + time.tv_usec;
  #endif
}

//
void TimeMeasurement::waitUS(uint64_t usec)
{
    uint64_t stop_time_usec = TimeMeasurement::getCurrentTimeUS() + usec;

    while (getCurrentTimeUS() < stop_time_usec);
}

} // namespace A10CC
