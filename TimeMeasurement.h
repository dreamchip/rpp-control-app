/******************************************************************************
*
* Copyright 2018,2019, Dream Chip Technologies GmbH. All rights reserved.
* No part of this work may be reproduced, modified, distributed, transmitted,
* transcribed, or translated into any language or computer format, in any form
* or by any means without written permission of:
* Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
* Germany
*
*****************************************************************************/
/**
* @file   time_measurement.h
*
* @brief  
*
*****************************************************************************/
#ifndef TIME_MEASUREMENT_H
#define TIME_MEASUREMENT_H

#include "inttypes.h"

#define US_PER_MS       (1000)
#define MS_PER_S        (1000)
#define US_PER_S        (1000*1000)
#define SEC_TO_US(x)    ((uint64_t) ((uint64_t) US_PER_S  * x))
#define SEC_TO_MS(x)    ((uint64_t) ((uint64_t) MS_PER_S  * x))
#define MS_TO_US(x)     ((uint64_t) ((uint64_t) US_PER_MS * x))
#define US_TO_MS(x)     ((uint64_t) (x / US_PER_MS))


namespace DCT
{


class TimeMeasurement
{
  public:
    TimeMeasurement();
    ~TimeMeasurement() = default;

    void     startMeasurement();
    uint64_t getMicrosecondsSinceStart();
    int64_t  setMicrosecondsSinceStart(uint64_t usec);   // for synchronization with other modules
    void     waitUntilUS(uint64_t usec);

    static void     waitUntilAbsUS(uint64_t usec);
    static uint64_t getCurrentTimeUS();
    static void     waitUS(uint64_t usec);

  private:
    enum {
        MICROSECONDS_PER_SECOND = 1000 * 1000,
    };

    const uint32_t  m_CLASS_ID;
    uint64_t        m_start_time_us; // microseconds

    static uint64_t m_ticks_per_sec;
    static bool     m_initialized;
};


} // namespace A10CC

#endif // #ifndef TIME_MEASUREMENT_H
