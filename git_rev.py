#!/usr/bin/env python
"""
Generate a C version header file by collecting the current Git commit ID,
modified state, revision date and branch of a (set of) directories.
"""
from __future__ import division, unicode_literals, print_function
import sys, os, argparse, glob, subprocess, shutil


def run(cmd, capture=True, verbose=False, may_fail=False):
    if verbose:
        print(os.getenv("PS4", "+ ") + " ".join(('"{}"'.format(arg) if (' ' in arg) else arg) for arg in cmd), file=sys.stderr)
    try:
        if capture:
            return subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0].decode('ascii', 'replace')
        else:
            return subprocess.call(cmd)
    except EnvironmentError as e:
        if may_fail:
            return ("" if capture else -1)
        print("FATAL: failed to run git command:", e, file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("dirs", metavar="DIR", nargs='*',
                        help="directories to check for changes [default: whole repository]")
    parser.add_argument("-o", "--output", metavar="FILE",
                        help="set output file name [default: stdout]")
    parser.add_argument("-p", "--prefix", default="GIT_",
                        help="set prefix for generated #defines [default: '%(default)s']")
    parser.add_argument("-f", "--date-format", metavar="FMT", default="%y%m%d",
                        help="set date format [default: '%(default)s']")
    parser.add_argument("-l", "--hash-length", metavar="N",
                        help="set hash length [default: Git --abbrev-commit]")
    parser.add_argument("-b", "--base-dir", metavar="DIR", default="",
                        help="root directory of the git checkout [default: current directory]")
    parser.add_argument("-v", "--verbose", action='store_true',
                        help="verbose operation; show all performed commands")
    args = parser.parse_args()

    have_static = args.output and os.path.isfile(args.output + ".static") \
                  and not(os.path.isdir(os.path.join(args.base_dir, ".git")))

    # glob-expand directories (if the shell didn't already do that)
    dirs = []
    for d in args.dirs:
        dirs += glob.glob(d)

    # get revision number and date
    cmd = ["git", "log", "-n", "1", "--format=%h%n%cd", "--date=format:" + args.date_format]
    if args.hash_length: cmd += ["--abbrev=" + str(args.hash_length)]
    cmd += ["HEAD"] + dirs
    res = run(cmd, verbose=args.verbose, may_fail=have_static)
    try:
        rev, date = [x.strip() for x in res.split('\n', 1)]
    except ValueError:
        if have_static:
            # this is not a Git checkout, but we have a static version file => use that
            try:
                shutil.copy2(args.output + ".static", args.output)
                sys.exit(0)
            except EnvironmentError as e:
                print("FATAL: could not copy static version file -", e, file=sys.stderr)
                sys.exit(1)
        else:
            print("FATAL: invalid output from `git log`", file=sys.stderr)
            sys.exit(1)

    # any uncommited changes?
    if run(["git", "diff", "--quiet", "--exit-code"] + dirs, capture=False, verbose=args.verbose):
        rev += "+"

    # get branch name
    branch = run(["git", "rev-parse", "--abbrev-ref", "HEAD"], verbose=args.verbose).strip()

    # compile output file
    result = ( '#define {prefix}REV    "{rev}"\n' \
             + '#define {prefix}DATE   "{date}"\n' \
             + '#define {prefix}BRANCH "{branch}"\n' \
             ).format(prefix=args.prefix, rev=rev, date=date, branch=branch)

    # produce output
    if not(args.output) or (args.output == "-"):
        sys.stdout.write(result)
    else:
        try:
            with open(args.output, 'rb') as f:
                old_contents = f.read().strip().replace(b'\r', b'')
            rewrite = (old_contents != result.strip().encode('ascii', 'replace'))
        except EnvironmentError:
            rewrite = True
        if rewrite:
            try:
                with open(args.output, 'w') as f:
                    f.write(result)
            except EnvironmentError as e:
                print("FATAL: could not write output file -", e, file=sys.stderr)
                sys.exit(1)
        elif args.verbose:
            print(args.output, "not changed, not rewriting it.", file=sys.stderr)
