TEMPLATE = app
TARGET = rpp-control-app

QT += widgets
QT += quick
QT += quickwidgets

requires(qtConfig(filedialog))

HEADERS += \
    ClassIDs.h \
    Function.h \
    HistogramWidget.h \
    ImageGridWidget.h \
    Instance.h \
    ListModel.h \
    Logger.h \
    LogString.h \
    MainWindow.h \
    OpenGLImageWidget.h \
    Register.h \
    Rest.h \
    RppFunctionsModel.h \
    RppRegistersModel.h \
    Schema.h \
    ServerConfig.h \
    ServerIdent.h \
    Settings.h \
    SettingsDialog.h \
    StyleSheetTCO.h \
    TableModel.h \
    TimeMeasurement.h \

SOURCES += \
    Function.cpp \
    HistogramWidget.cpp \
    ImageGridWidget.cpp \
    Instance.cpp \
    ListModel.cpp \
    Logger.cpp \
    Main.cpp \
    MainWindow.cpp \
    OpenGLImageWidget.cpp \
    Register.cpp \
    Rest.cpp \
    RppFunctionsModel.cpp \
    RppRegistersModel.cpp \
    Schema.cpp \
    ServerConfig.cpp \
    ServerIdent.cpp \
    Settings.cpp \
    SettingsDialog.cpp \
    TableModel.cpp \
    TimeMeasurement.cpp \

win32 {

    LIBS += -lGdiplus
    DEFINES += _CRT_SECURE_NO_WARNINGS
    QMAKE_CFLAGS_DEBUG += -O0

    RC_ICONS = DCTlogo2.ico
}

RESOURCES += rpp_control_app.qrc

target.path = rpp-control-app

INSTALLS += target

DISTFILES +=

FORMS +=

SUBDIRS += quickwidget.pro
